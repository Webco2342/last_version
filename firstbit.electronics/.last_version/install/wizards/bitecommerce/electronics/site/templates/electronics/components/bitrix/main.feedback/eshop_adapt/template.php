<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

	<?if(!empty($arResult["ERROR_MESSAGE"]))
	{
		foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError($v);
	}
	if(strlen($arResult["OK_MESSAGE"]) > 0)
	{
		?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
	}
	?>
	<form class="register-form" role="form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
		<div class="col-md-6">
		
			<div class="form-group">
		    <label class="info-title" for="exampleInputName"><?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span>*</span><?endif?></label>
		    <input type="text" class="form-control unicase-form-control text-input" name="user_name" id="exampleInputName" value="<?=$arResult["AUTHOR_NAME"]?>">			
		  </div>
		
		</div>
	<div class="col-md-6">
		
			<div class="form-group">
		    <label class="info-title" for="exampleInputEmail1"><?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span>*</span><?endif?></label>
		    <input type="email" class="form-control unicase-form-control text-input" name="user_email" id="exampleInputEmail1" value="<?=$arResult["AUTHOR_EMAIL"]?>">
		  </div>
		
	</div>		 
	<div class="col-md-12">
		
			<div class="form-group">
		    <label class="info-title" for="exampleInputComments"><?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span>*</span><?endif?></label>
		    <textarea class="form-control unicase-form-control" name="MESSAGE" id="exampleInputComments"><?=$arResult["MESSAGE"]?></textarea>
		  </div>
		
</div>


		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<strong><?=GetMessage("MFT_CAPTCHA")?></strong><br/>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA"><br/>
			<strong><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></strong><br/>
			<input type="text" name="captcha_word" size="30" maxlength="50" value=""/><br/>
		<?endif;?>
	<div class="col-md-12 outer-bottom-small m-t-20">
		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		<input type="submit" name="submit" style="color:#fff" value="<?=GetMessage("MFT_SUBMIT")?>" class="btn-upper btn btn-primary checkout-page-button">
	</div>

	</form>
