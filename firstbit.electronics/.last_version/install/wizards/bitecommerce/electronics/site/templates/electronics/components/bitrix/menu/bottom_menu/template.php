<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>

		<?
		$i=0;
		$count=count($arItem);
		foreach($arResult as $itemIdex => $arItem):
		$count--;
		?>
			<?if($i=="0"){?>
			<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="module-heading">
                        <h4 class="module-title"><? echo GetMessage('BOTOM_MEDNU_TITLE'); ?></h4>
                    </div><!-- /.module-heading -->
                    <div class="module-body bottom_menu">
                        <ul class='list-unstyled'>			
			<?}?>
				<li class="bx-inclinksfooter-item"><a href="<?=$arItem["LINK"]?>"><?=htmlspecialcharsbx($arItem["TEXT"])?></a></li>
				
			<?if($count==0){?>
			                        </ul>
                    </div></div><!-- /.module-body -->
					<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="module-heading">
                        <h4 class="module-title"></br></h4>
                    </div><!-- /.module-heading -->
                    <div class="module-body bottom_menu">
                        <ul class='list-unstyled'>	
					
			<?}elseif($i=="3"){?>
			
			                        </ul>
                    </div></div><!-- /.module-body -->
					<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="module-heading">
                        <h4 class="module-title"></br></h4>
                    </div><!-- /.module-heading -->
                    <div class="module-body bottom_menu">
                        <ul class='list-unstyled'>				
			<?}?>
			
		<?
		$i++;
		endforeach;?>
                        </ul>
                    </div></div><!-- /.module-body -->
