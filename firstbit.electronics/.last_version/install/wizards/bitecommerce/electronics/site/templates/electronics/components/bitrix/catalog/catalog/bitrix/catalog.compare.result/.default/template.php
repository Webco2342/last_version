<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

?>
		<div class="row product-comparison inner-bottom-sm" id="bx_catalog_compare_block">
			<h1 class="page-title text-center"><?=GetMessage("SRAV_TITLE"); ?></h1>
<?
if ($isAjax)
{
	$APPLICATION->RestartBuffer();
}
?>
			<div class="table-responsive">
				<table class="table table-bordered compare-table inner-top-vs">
<?
if (!empty($arResult["SHOW_FIELDS"]))
{
	foreach ($arResult["SHOW_FIELDS"] as $code => $arProp)
	{
		$showRow = true;
		if (!isset($arResult['FIELDS_REQUIRED'][$code]) || $arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$arPropertyValue = $arElement["FIELDS"][$code];
				if (is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}
		if ($showRow)
		{
			if($code != "DETAIL_PICTURE"){?><tr><th width="250px"><?if($code == "NAME"){?><?=GetMessage("TOV_TITLE"); ?><?}elseif($code == "PREVIEW_TEXT"){?><?=GetMessage("PREVIEW_TEXT_TITLE"); ?><?}else{?><?=GetMessage("IBLOCK_FIELD_".$code);?><?}?></th><?
			foreach($arResult["ITEMS"] as &$arElement)
			{
		?>
				<td style="max-width:350px; white-space: inherit;" valign="top">
				<div class="product">
				<div class="product-image">
				
				
		<?
				switch($code)
				{
					case "NAME":						
						if(is_array($arElement["FIELDS"]["DETAIL_PICTURE"])):?>
						<div class="image">
							<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img
							border="0"
							src="<?=$arElement["FIELDS"]["DETAIL_PICTURE"]["SRC"]?>"
							width="auto"
							height="150"
							alt="<?=$arElement["FIELDS"]["DETAIL_PICTURE"]["ALT"]?>"
							title="<?=$arElement["FIELDS"]["DETAIL_PICTURE"]["TITLE"]?>"
							/></a>
						</div>
						<?endif;?>
						<div class="product-info text-left">
						<h3 class="name"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement[$code]?></a></h3>
						<?if($arElement["CAN_BUY"]):?>
						<noindex><div class="action"><a class="nk btn btn-primary" href="<?=$arElement["BUY_URL"]?>" rel="nofollow"><?=GetMessage("CATALOG_COMPARE_BUY"); ?></a></div></noindex>
						<?elseif(!empty($arResult["PRICES"]) || is_array($arElement["PRICE_MATRIX"])):?>
						<br /><?=GetMessage("CATALOG_NOT_AVAILABLE")?>
						<?endif;
						break;?>
						</div>
						<?
					case "PREVIEW_PICTURE":
					case "DETAIL_PICTURE":
					
						if(is_array($arElement["FIELDS"][$code])):?>
						<div class="image">
							<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img
							border="0"
							src="<?=$arElement["FIELDS"][$code]["SRC"]?>"
							width="auto"
							height="150"
							alt="<?=$arElement["FIELDS"][$code]["ALT"]?>"
							title="<?=$arElement["FIELDS"][$code]["TITLE"]?>"
							/></a>
						</div>
						<?endif;?>
					<?
						break;
					default:
						echo '<p class="text">'.$arElement["FIELDS"][$code].'</p>';
						break;
				}
			?>
			</div>
			</div>
				</td>
			<?
			}
			unset($arElement);
		
	?>
	</tr>
	<?}?>
		<?}
	}
}

if (!empty($arResult["SHOW_OFFER_FIELDS"]))
{
	foreach ($arResult["SHOW_OFFER_FIELDS"] as $code => $arProp)
	{
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$Value = $arElement["OFFER_FIELDS"][$code];
				if(is_array($Value))
				{
					sort($Value);
					$Value = implode(" / ", $Value);
				}
				$arCompare[] = $Value;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}
		if ($showRow)
		{
		?>
		<tr>
			<th><?=GetMessage("IBLOCK_OFFER_FIELD_".$code)?></th>
			<?foreach($arResult["ITEMS"] as &$arElement)
			{
			?>
			<td>
				<?=(is_array($arElement["OFFER_FIELDS"][$code])? implode("/ ", $arElement["OFFER_FIELDS"][$code]): $arElement["OFFER_FIELDS"][$code])?>
			</td>
			<?
			}
			unset($arElement);
			?>
		</tr>
		<?
		}
	}
}
?>
<tr>
	<th><?=GetMessage('CATALOG_COMPARE_PRICE');?></th>
	<?
	foreach ($arResult["ITEMS"] as &$arElement)
	{
		if (isset($arElement['MIN_PRICE']) && is_array($arElement['MIN_PRICE']))
		{
			?><td><div class="product-price">
								<span class="price"> 
								<? echo $arElement['MIN_PRICE']['PRINT_DISCOUNT_VALUE']; ?>
								</span>
							</div></td><?
		}
		else
		{
			?><td>&nbsp;</td><?
		}
	}
	unset($arElement);
	?>
</tr>
<?
if (!empty($arResult["SHOW_PROPERTIES"]))
{
	foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
	{
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
				if (is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}

		if ($showRow)
		{
			?>
			<tr>
				<th><?=$arProperty["NAME"]?></th>
				<?foreach($arResult["ITEMS"] as &$arElement)
				{
					?>
					<td>
						<p class="text"><?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></p>
					</td>
				<?
				}
				unset($arElement);
				?>
			</tr>
		<?
		}
	}
}

if (!empty($arResult["SHOW_OFFER_PROPERTIES"]))
{
	foreach($arResult["SHOW_OFFER_PROPERTIES"] as $code=>$arProperty)
	{
		$showRow = true;
		if ($arResult['DIFFERENT'])
		{
			$arCompare = array();
			foreach($arResult["ITEMS"] as &$arElement)
			{
				$arPropertyValue = $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["VALUE"];
				if(is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			unset($arElement);
			$showRow = (count(array_unique($arCompare)) > 1);
		}
		if ($showRow)
		{
		?>
		<tr>
			<th><?=$arProperty["NAME"]?></th>
			<?foreach($arResult["ITEMS"] as &$arElement)
			{
			?>
			<td><p class="text">
				<?=(is_array($arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["OFFER_DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></p>
			</td>
			<?
			}
			unset($arElement);
			?>
		</tr>
		<?
		}
	}
}
	?>
	<tr>
		<th><?=GetMessage("CATALOG_REMOVE_PRODUCT")?></th>
		<?foreach($arResult["ITEMS"] as &$arElement)
		{
		?>
		<td class='text-center'>
			<a class="remove-icon" onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="javascript:void(0)"><i class="fa fa-times"></i></a>
		</td>
		<?
		}
		unset($arElement);
		?>
	</tr>
</table>
</div>
<?
if ($isAjax)
{
	die();
}
?>
</div>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>