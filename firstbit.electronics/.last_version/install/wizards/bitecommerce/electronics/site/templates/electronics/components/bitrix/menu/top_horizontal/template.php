<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;





$menuBlockId = "catalog_menu_".$this->randString();
?>
<div class="yamm navbar navbar-default" role="navigation" id="<?=$menuBlockId?>">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
	<div class="nav-outer">
		<ul class="nav navbar-nav">
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
			<li
				class="dropdown yamm-fw <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>active<?endif?> <?=$arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']?>"				
			>
				<a 
					href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
					<?if (is_array($arColumns) && count($arColumns) > 0):?>						
						class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown"
					<?endif?>
				>
					<span>
						<?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?>
						<?if (is_array($arColumns) && count($arColumns) > 0):?><i class="fa fa-angle-down"></i><?endif?>
					</span>
				</a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<span class="bx-nav-parent-arrow" onclick="obj_<?=$menuBlockId?>.toggleInMobile(this)"></span> <!-- for mobile -->				
					<?foreach($arColumns as $key=>$arRow):?>
						<ul class="dropdown-menu pages" style="<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']=='catalog'){?>left:0;right:0;<?}?>">
						<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->						
							<li class= "menu_title">
							<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']=='catalog'){?><div class="col-xs-12 col-sm-6 col-md-4" style="min-height: 100px;"><?}?>
								<a
									href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
									<?if ($existPictureDescColomn):?>
										ontouchstart="//document.location.href = '<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>';"
										onmouseover="obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_2?>');"
									<?endif?>									
								>
									<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']=='catalog'){?><b><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></b><?}?>
									<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']!='catalog'){?><span ><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></span><?}?>
									
								</a>
							
							<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
								
								<?
								$i = 0;
								foreach($arLevel_3 as $itemIdLevel_3):
								if($i<5){
								?>	<!-- third level-->
									
										<a style="display: inline-block; margin-right: 8px;"
											href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>"
											<?if ($existPictureDescColomn):?>
												ontouchstart="//document.location.href = '<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>';return false;"
												onmouseover="obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_3?>');return false;"
											<?endif?>
											data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"]?>">
											<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?>
										</a>
									
								<?
								};
								++$i;
								endforeach;?>
								
							<?endif?>
							<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']=='catalog'){?></div><?}?>
							</li>
							
						<?endforeach;?>
						</ul>
					<?endforeach;?>
				
			<?endif?>
			</li>
		<?endforeach;?>
		</ul><!-- /.navbar-nav -->
		<div class="clearfix"></div>				
	</div><!-- /.nav-outer -->
</div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->

