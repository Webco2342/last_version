<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="checkout-box faq-page inner-bottom-sm">
				<div class="col-md-12">
					<h2><? echo GetMessage('TITLE_OTZ'); ?></h2>
					<div class="panel-group checkout-steps" id="accordion">

<?$i= 1;
foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="panel panel-default checkout-step-0<?=$i?>">

	<!-- panel-heading -->
		<div class="panel-heading">
    	<h4 class="unicase-checkout-title">
	        <a data-toggle="collapse" class="<?if($i!=1){?>collapsed<?}?>" data-parent="#accordion" href="#collapse<?=$i?>">
	          <span><?=$i?></span><?echo $arItem["NAME"]?>
	        </a>
	     </h4>
    </div>
    <!-- panel-heading -->

	<div id="collapse<?=$i?>" class="panel-collapse collapse <?if($i==1){?>in<?}?>">

		<!-- panel-body  -->
	    <div class="panel-body">
	    	<?echo $arItem["PREVIEW_TEXT"];?>	
		</div>
		<!-- panel-body  -->

	</div><!-- row -->
</div>
<?++$i?>
<?endforeach;?>
</div>
</div>
</div>