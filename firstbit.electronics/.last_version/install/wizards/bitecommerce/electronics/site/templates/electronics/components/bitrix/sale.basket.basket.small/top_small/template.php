<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="dropdown dropdown-cart">
<style > .rub{ display: inline-block; font-size: 13px; line-height: 4px; border-bottom: 1px solid #abd07e; width:0.4em; } </style>
<?
if ($arResult["READY"]=="Y" || $arResult["DELAY"]=="Y" || $arResult["NOTAVAIL"]=="Y" || $arResult["SUBSCRIBE"]=="Y")
{

	if ($arResult["READY"]=="Y")
	{
	$coount = count($arResult["ITEMS"]);	
		foreach ($arResult["ITEMS"] as $sum)
		{
		$SUM += $sum['PRICE']*$sum['QUANTITY'];
		}	
		$SUM = number_format($SUM, 2, ',', ' ');
		?>

		
			<a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
			<div class="items-cart-inner">
				<div class="total-price-basket">
					<span class="lbl"><?= GetMessage("TSBS_COR") ?> -</span>
					<span class="total-price">						
						<span class="value">&nbsp;<?=$SUM?></span>
						<span class="sign rub" ><div>P</div> </span>
					</span>
				</div>
				<div class="basket">
					<i class="glyphicon glyphicon-shopping-cart"></i>
				</div>
				<div class="basket-item-count"><span class="count"><?=$coount?></span></div>
			
		    </div>
		</a>	
<ul id="bascet" class="dropdown-menu"><?
		foreach ($arResult["ITEMS"] as &$v)
		{
		$res = CIBlockElement::GetByID($v['PRODUCT_ID']);
		if($ar_res = $res->GetNext()){		
		$IMG=CFile::GetPath ($ar_res["DETAIL_PICTURE"]);	
		if(!$IMG){
		$res = CIBlockElement::GetProperty($ar_res['IBLOCK_ID'], $ar_res['ID'], "sort", "asc", array("CODE" => "CML2_LINK"));
		if ($ob = $res->GetNext())
		{
		$res = CIBlockElement::GetByID($ob['VALUE']);
		if($ar_res = $res->GetNext()){		
		$IMG=CFile::GetPath ($ar_res["DETAIL_PICTURE"]);	
		}        
		}
		}
		}
		 
		$format = number_format($v['PRICE'], 2, ',', ' ');
			if ($v["DELAY"]=="N" && $v["CAN_BUY"]=="Y")
			{
				?><li>
				
				<div class="cart-item product-summary">
					<div class="row">
						<div class="col-xs-4">
							<div class="image">
								<a href="<?echo $v["DETAIL_PAGE_URL"]; ?>"><img width="47" src="<?=$IMG?>" alt=""></a>
							</div>
						</div>
						<div class="col-xs-7">
							
							<h3 class="name"><?
							if ('' != $v["DETAIL_PAGE_URL"])
							{
								?><a href="<?echo $v["DETAIL_PAGE_URL"]; ?>"><?echo $v["NAME"]?></a><?
							}
							else
							{
								?><?echo $v["NAME"]?><?
							}
							?></h3>
							<div class="price"><?echo $format?><span class="rub" ><div>P</div> </span></div>
							<div class="price"><?echo $v["QUANTITY"]?> <?= GetMessage("TSBS_ED") ?></div>
						</div>
						<div class="col-xs-1 action">
							<a href="?DELET_ID=<?=$v["ID"]?>"><i class="fa fa-trash"></i></a>
						</div>
					</div>
				</div><!-- /.cart-item -->
				<div class="clearfix"></div>
			<hr>			
				<?
				if($_GET['DELET_ID']==$v["ID"]){
			if (CSaleBasket::Delete($_GET['DELET_ID']));
								?>
								<script>
								setTimeout(function() {	
								 BX.ajax.insertToNode('<?=SITE_DIR?>include/bascet.php', 'bascet');
								 }, 10);
								 </script>
								 <?
				}		
			}
		}
		if (isset($v))
			unset($v);
		?><?
		if ('' != $arParams["PATH_TO_BASKET"])
		{
			?><form method="get" action="<?=$arParams["PATH_TO_BASKET"]?>">
			<input class="btn btn-upper btn-primary btn-block m-t-20" type="submit" value="<?= GetMessage("TSBS_2BASKET") ?>">
			</form><?
		}?>
		</ul>
	<?}
	if ($arResult["DELAY"]=="Y")
	{
		?><tr><td align="center"><?= GetMessage("TSBS_DELAY") ?></td></tr>
		<tr><td><ul>
		<?
		foreach ($arResult["ITEMS"] as &$v)
		{
			if ($v["DELAY"]=="Y" && $v["CAN_BUY"]=="Y")
			{
				?><li><?
				if ('' != $v["DETAIL_PAGE_URL"])
				{
					?><a href="<?echo $v["DETAIL_PAGE_URL"] ?>"><b><?echo $v["NAME"]?></b></a><?
				}
				else
				{
					?><b><?echo $v["NAME"]?></b><?
				}
				?><br />
				<?= GetMessage("TSBS_PRICE") ?>&nbsp;<b><?echo $v["PRICE_FORMATED"]?></b><br />
				<?= GetMessage("TSBS_QUANTITY") ?>&nbsp;<?echo $v["QUANTITY"]?>
				</li>
				<?
			}
		}
		if (isset($v))
			unset($v);
		?></ul></td></tr><?
		if ('' != $arParams["PATH_TO_BASKET"])
		{
			?><tr><td><form method="get" action="<?=$arParams["PATH_TO_BASKET"]?>">
			<input type="submit" value="<?= GetMessage("TSBS_2BASKET") ?>">
			</form></td></tr><?
		}
	}
	if ($arResult["SUBSCRIBE"]=="Y")
	{
		?><tr><td align="center"><?= GetMessage("TSBS_SUBSCRIBE") ?></td></tr>
		<tr><td><ul><?
		foreach ($arResult["ITEMS"] as &$v)
		{
			if ($v["CAN_BUY"]=="N" && $v["SUBSCRIBE"]=="Y")
			{
				?><li><?
				if ('' != $v["DETAIL_PAGE_URL"])
				{
					?><a href="<?echo $v["DETAIL_PAGE_URL"] ?>"><b><?echo $v["NAME"]?></b></a><?
				}
				else
				{
					?><b><?echo $v["NAME"]?></b><?
				}
				?></li><?
			}
		}
		if (isset($v))
			unset($v);
		?></ul></td></tr><?
	}
	if ($arResult["NOTAVAIL"]=="Y")
	{
		?><tr><td align="center"><?= GetMessage("TSBS_UNAVAIL") ?></td></tr>
		<tr><td><ul><?
		foreach ($arResult["ITEMS"] as &$v)
		{
			if ($v["CAN_BUY"]=="N" && $v["SUBSCRIBE"]=="N")
			{
				?><li><?
				if ('' != $v["DETAIL_PAGE_URL"])
				{
					?><a href="<?echo $v["DETAIL_PAGE_URL"] ?>"><b><?echo $v["NAME"]?></b></a><?
				}
				else
				{
					?><b><?echo $v["NAME"]?></b><?
				}
				?><br />
				<?= GetMessage("TSBS_PRICE") ?>&nbsp;<b><?echo $v["PRICE_FORMATED"]?></b><br />
				<?= GetMessage("TSBS_QUANTITY") ?>&nbsp;<?echo $v["QUANTITY"]?>
				</li><?
			}
		}
		if (isset($v))
			unset($v);
		?></ul></td></tr><?
	}

}else{
	?>
	
	<a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
			<div class="items-cart-inner">
			<div class="total-price-basket">
					<span class="lbl"><?= GetMessage("TSBS_COR") ?> -</span>
					<span class="total-price">						
						<span class="value">0.00</span>
						<span class="sign rub" ><div>P</div> </span>
					</span>
			</div>
			</div>
	</a>
	<?
	}
?>
</div>
