<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="info-boxes wow fadeInUp">
	<div class="info-boxes-inner">
			<div class="row">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?><a href="<?=$arItem['PROPERTIES']['HTTP']['VALUE']?>"><?endif;?>
			<div class="col-md-6 col-sm-4 col-lg-4">
				<div class="info-box" style="text-align: center;">
					<div class="row">
						<div class="col-xs-12" >
						     <i style="color: #000;" class="icon fa fa-<?=$arItem['PROPERTIES']['CODE_ICON']['VALUE']?>"></i>
									<h4 class="info-box-heading <?=$arItem['PROPERTIES']['COLOR']['VALUE']?>"><?echo $arItem["NAME"]?></h4></div>
					</div>	
					<h6 class="text"><?echo $arItem["PREVIEW_TEXT"];?></h6>
				</div>
			</div><!-- .col -->
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?></a><?endif;?>
<?endforeach;?>
</div>
</div>
</div>
