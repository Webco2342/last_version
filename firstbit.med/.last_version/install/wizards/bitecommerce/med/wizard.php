<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
	function InitStep()
	{
		parent::InitStep();

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "med";
	}
}

class SelectTemplateStep extends CSelectTemplateWizardStep
{
}

class SelectThemeStep extends CSelectThemeWizardStep
{
}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "med";
		parent::InitStep();

		$templateID = $wizard->GetVar("templateID");
		$themeID = $wizard->GetVar($templateID."_themeID");
        $templatePath = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".$templateID."/";
        $wizardTemplatePath = "/bitrix/wizards/bitecommerce/med/site/templates/".$templateID;
		$themePath = $wizardTemplatePath."/themes/".$themeID;

        $siteLogo = $themePath."/images/logo.png";
		$wizard->SetDefaultVars(
			Array(
				"siteLogo" => $siteLogo,	
				"siteCopyrightText" => $this->GetFileContent(WIZARD_SITE_PATH."include/copyright.php", GetMessage("WIZ_COPYRIGHT_DEF")),
				"siteHeaderContactMail" => $this->GetFileContent(WIZARD_SITE_PATH."include/kontakt_mail_header.php", GetMessage("WIZ_MAIL_DEF")),
				"siteHeaderContactTel" => $this->GetFileContent(WIZARD_SITE_PATH."include/kontakt_tel_header.php", GetMessage("WIZ_TEL_DEF")),
			)
		);
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
        
        $siteLogo = $wizard->GetVar("siteLogo", true);
        //echo $siteLogo;
        $siteLogoShow = CFile::ShowImage($siteLogo, 186, 51);

		$this->content .= '<table width="100%" cellspacing="0" cellpadding="0">';
        
        $this->content .= '<tr><td><h2>'.GetMessage("SETTINGS_MAIN").'</h2></tr></td>';
        
        $this->content .= '<tr><td>';
        $this->content .= '<strong>'.GetMessage("WIZ_LOGO").'</strong><br />';
        $this->content .= $siteLogoShow."<br/>";
        $this->content .= $this->ShowFileField("siteLogo", Array("show_file_info" => "N", "id" => "site-logo"));
        $this->content .= '<p style="margin-top:0!important;">'.GetMessage("WIZ_LOGO_DESC").'</p>';
        $this->content .= '</tr></td>';
		$this->content .= '<tr><td><br /></td></tr>';
        
        //Header site
        $this->content .= '<tr><td>';
        $this->content .= '<strong>'.GetMessage("WIZ_HEADER").'</strong><br />';
        $this->content .= $siteHeaderShow."<br/>";
        $this->content .= '<p style="margin-top:0!important;">'.GetMessage("WIZ_HEADER_DESC").'</p>';
        $this->content .= '</tr></td>';
		$this->content .= '<tr><td><br /></td></tr>';
        
     
        //Copyright
		$this->content .= '<tr><td>';		
		$this->content .= '<label for="WIZ_COPYRIGHT"><strong>'.GetMessage("WIZ_COPYRIGHT").'</strong></label><br />';
		$this->content .= $this->ShowInputField("textarea", "siteCopyrightText", Array("id" => "WIZ_COPYRIGHT", "style" => "width:100%", "rows"=>"3"));
		$this->content .= '</tr></td>';

        //Contact		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="WIZ_HEADER_CONTACT_MAIL"><strong>'.GetMessage("WIZ_HEADER_CONTACT_MAIL").'</strong></label><br />';
		$this->content .= $this->ShowInputField("text", "siteHeaderContactMail", Array("id" => "WIZ_HEADER_CONTACT_MAIL", "style" => "width:100%", "rows"=>"3"));	
		$this->content .= '</tr></td>';		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="WIZ_HEADER_CONTACT_TEL"><strong>'.GetMessage("WIZ_HEADER_CONTACT_TEL").'</strong></label><br />';
		$this->content .= $this->ShowInputField("text", "siteHeaderContactTel", Array("id" => "WIZ_HEADER_CONTACT_TEL", "style" => "width:100%", "rows"=>"3"));	
		$this->content .= '</tr></td>';	
		
		$this->content .= '<tr><td><br /></td></tr>';
     		$this->content .= '</table>';
                
		$this->content .= $this->ShowHiddenField("installDemoData","Y");
		
		$formName = $wizard->GetFormName();
		$installCaption = $this->GetNextCaption();
		$nextCaption = GetMessage("NEXT_BUTTON");
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();
        $res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 51, "max_width" => 186));
	

	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function CorrectServices(&$arServices)
	{
		$wizard =& $this->GetWizard();
		if($wizard->GetVar("installDemoData") != "Y")
		{
		}
	}
}

class FinishStep extends CFinishWizardStep
{
}
?>