<?
$arUrlRewrite = array(
	
array(
		"CONDITION" => "#^#SITE_DIR#departament/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#departament/index.php",
	),

array(
		"CONDITION" => "#^#SITE_DIR#services/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#services/index.php",
	),	
	
array(
		"CONDITION" => "#^#SITE_DIR#personal/([0-9]+)/([0-9]+)/(.*)#",
		"RULE" => "SECTION_ID=\$1&ELEMENT_ID=\$2",
		"ID" => "",
		"PATH" => "#SITE_DIR#personal/detail.php",
	),
	
array(
		"CONDITION" => "#^#SITE_DIR#personal/([0-9]+)/(.*)#",
		"RULE" => "SECTION_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#personal/index.php",
	),	

array(
		"CONDITION" => "#^#SITE_DIR#news/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#news/detail.php",
	),

array(
		"CONDITION" => "#^#SITE_DIR#contact/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#contact/index.php",
	),	
	
array(
		"CONDITION" => "#^#SITE_DIR#patient/section/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#patient/section/detail.php",
	),	
	

array(
		"CONDITION" => "#^#SITE_DIR#patient/promotions/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#patient/promotions/detail.php",
	),	
array(
		"CONDITION" => "#^#SITE_DIR#blogs/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#blogs/detail.php",
	),	
array(
		"CONDITION" => "#^#SITE_DIR#detail/([0-9]+)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "#SITE_DIR#detail.php",
	),
);


?>