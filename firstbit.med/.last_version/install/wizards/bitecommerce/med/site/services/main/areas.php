<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


function ___writeToAreasFile($fn, $text)
{
	if(file_exists($fn) && !is_writable($abs_path) && defined("BX_FILE_PERMISSIONS"))
		@chmod($abs_path, BX_FILE_PERMISSIONS);

	$fd = @fopen($fn, "wb");
	if(!$fd)
		return false;

	if(false === fwrite($fd, $text))
	{
		fclose($fd);
		return false;
	}

	fclose($fd);

	if(defined("BX_FILE_PERMISSIONS"))
		@chmod($fn, BX_FILE_PERMISSIONS);
}

	// LOGO
	$siteLogo = $wizard->GetVar("siteLogo");
	$sWizardTemplatePath = WizardServices::GetTemplatesPath(WIZARD_RELATIVE_PATH."/site")."/".WIZARD_TEMPLATE_ID."/";

	if($siteLogo > 0)
	{
		$file = CFile::GetByID($siteLogo);
		if($zr = $file->Fetch())
		{
			$strOldFile = str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$zr["SUBDIR"]."/".$zr["FILE_NAME"]);
			if(file_exists($strOldFile))
			{
				$oldLogoFile = $_SERVER['DOCUMENT_ROOT']."/bitrix/templates/".WIZARD_TEMPLATE_ID."_".WIZARD_THEME_ID."_".WIZARD_SITE_ID."/images/logo.png";
				@unlink($oldLogoFile);
				$newLogoFile = $_SERVER['DOCUMENT_ROOT']."/bitrix/templates/".WIZARD_TEMPLATE_ID."_".WIZARD_THEME_ID."_".WIZARD_SITE_ID."/images/logo.".end(explode(".", $zr["FILE_NAME"]));
				$newLogoPath = str_replace($_SERVER['DOCUMENT_ROOT'], '', $newLogoFile);
				@copy($strOldFile, $newLogoFile);
				___writeToAreasFile(WIZARD_SITE_PATH."include/logo.php", '<img src="'.$newLogoPath.'" />');
				CFile::Delete($siteLogo);
			}
		}
	}

CheckDirPath(WIZARD_SITE_DIR."include/");

$wizard =& $this->GetWizard();
	___writeToAreasFile(WIZARD_SITE_PATH."include/copyright.php", $wizard->GetVar("siteCopyrightText"));
	___writeToAreasFile(WIZARD_SITE_PATH."include/kontakt_mail_header.php", $wizard->GetVar("siteHeaderContactMail"));
	___writeToAreasFile(WIZARD_SITE_PATH."include/kontakt_tel_header.php", $wizard->GetVar("siteHeaderContactTel"));

			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/about/licenses/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/services/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/section/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/section/detail.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/promotions/detail.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/promotions/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/hospitalization/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/news/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/news/detail.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/blogs/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/blogs/detail.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/detail.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/hospitalization/index.php", array("EMIL_FORM" => $wizard->GetVar("siteHeaderContactMail")));

COption::SetOptionString("fileman", "show_untitled_styles", "Y");

?>