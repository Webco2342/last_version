<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php",
			"template.php",
			"theme.php",            
            "areas.php",
			"postevents.php",
		),
	),

	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK"),
		"STAGES" => Array(
			"types.php",
			"akc.php",
			"blogs.php",
			"demo_index_1.php",
			"deportament.php",
			"licenz.php",
			"news.php",
			"otz.php",
			"personal.php",
			"photo.php",
			"podpiski.php",
			"price.php",
			"section.php",
			"service.php",
			"slide_index.php",
			"form.php",
			"zapis_na_priem.php",
			"vacancies.php",
		),
	),
);
?>