<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
		

                 <div class="col-xs-12 col-sm-12 col-md-6">
                 
                    <div class="latest-post-wrap" >
					    <div class="subtitle col-xs-12 no-pad col-sm-11 col-md-12"><?echo GetMessage("TITLE")?></div>
						<?foreach($arResult["ITEMS"] as $arItem):?>
							<?
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>
                        <div class="post-item-wrap col-sm-6 col-md-12 col-xs-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="img-responsive post-author-img" alt="" />
                            	<div class="post-content1 pull-left col-md-9 col-sm-9 col-xs-8">
        	                        <div class="post-title pull-left"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME_IMPLODE']?></a></div>
        	                        <div class="post-meta-top pull-left">
        	                            <ul>
        	                            <li><i class="icon-calendar"></i><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></li>
        	                            
        	                            </ul>
        	                        </div>
                                </div>
                                <div class="post-content2">                   
                                	<p><?=$arItem['PREVIEW_TEXT_IMPLODE']?><br />
                                	<span class="post-meta-bottom"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo GetMessage("CT_BNL_GOTO_DETAIL")?></a></span></p>
                         		</div>
                         </div>
						 
						 


						<?endforeach;?>
                         <a href="<?=SITE_DIR?>news/" class="dept-details-butt posts-showall"><?echo GetMessage("CT_BNL_GOTO_NEWS")?></a>
                            
                        </div>
                    </div>
