<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (!empty($arResult)):?>
<div style="height:66px;display: block;position: relative;">
                    <div id="headerstic" style="position: relative;">
                    
                    <div class=" top-bar container">
                    	<div class="row">
                            <nav class=" navbar-default" >
                              <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          
                          <button type="button" class="navbar-toggle icon-list-ul" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Переключить меню</span>
                          </button>
                          <button type="button" class="navbar-toggle icon-rocket" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                            <span class="sr-only">Переключить меню</span>
                          </button>

                          <a href="<?=SITE_DIR?>"><div class="logo"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/logo.php"), false);?></div></a>
                        </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
<?
$previousLevel = 0;
$i=0;
foreach($arResult as $key=>$arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):
		$i++;
		?>
		<?if($i<=6){?>
			<li  class="<?if ($arItem["SELECTED"]):?>active<?else:?>dropdown<?endif?>"><a href="<?=$arItem["LINK"]?>" class="dropdown-toggle" ><?=$arItem["TEXT"]?></a>
				<ul class="dropdown-menu">
			<?}?>
		<?else:?>
			<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul class="dropdown-menu">
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):
			$i++;
			?>
			<?if($i<=6){?>
				<li class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>"><a href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a></li>
			<?}?>
			<?else:?>
				<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>
			
		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>"><a href="" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>
	<?endif?>



	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?
$i=0;
$len = sizeof($arResult); 
foreach($arResult as $key=>$arItem):?>
		<?if ($arItem["DEPTH_LEVEL"] == 1){
		$i++;
		?>
<?if($i==6){?>
<li class="dropdown dop-menu"><a style="width: 70px;"  class="dropdown-toggle" data-toggle="dropdown" ><span>...</span><b class="icon-angle-down"></b></a>
<ul class="dropdown-menu" style="top: 60px;">
<?}?>
<?if($i>6){?>
 <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
<?}?>
<?--$len;
if(!$len) {
?>
</ul>
</li>

		<?}}?>
<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                              </div><!-- /.container-fluid -->
                            </nav>
                    	</div>    
                    </div><!--Topbar End-->
                	</div>
<?endif?>
</div>