<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)	die();
	$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<?
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/jquery-ui-1.10.3.custom.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/animate.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/font-awesome.min.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/color.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/rs-plugin/css/settings.min.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/slides.css" >');
		$APPLICATION->addHeadString('<link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/inline.min.css" >');
		?>		
		<?$APPLICATION->addHeadString('<!--[if IE 9]>  <link rel="stylesheet" type="text/css" href="'.SITE_TEMPLATE_PATH.'/css/ie9.css" ><![endif]--> ');?>
		<?IncludeTemplateLangFile(__FILE__);?>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>images/faivcon.png" /> 	
	</head>
	<body>

		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
            <header>
            
            <div class="header-bg">
            <div id="search-overlay">
            <div class="container">
        						<div id="close">X</div>
								<?$APPLICATION->IncludeComponent(
									"bitrix:search.form",
									"search",
									Array(
										"COMPONENT_TEMPLATE" => ".default",
										"PAGE" => "#SITE_DIR#search/index.php",
										"USE_SUGGEST" => "N"
									)
								);?>
								</div>
			</div>
               
                	
                    <!--Topbar-->
                    <div class="topbar-info no-pad">                    
                        <div class="container">  
						    <div class="social-wrap-head col-md-3 no-pad"">
						<?
						$facebookLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_facebook.php");
						$twitterLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_twitter.php");
						$googlePlusLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_google.php");
						$linkedinLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_linkedin.php");
						$rssLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_rss.php");
						?>
                                <ul>
								<?if ($facebookLink):?>
                                <li><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_facebook.php"), false);?></li>
								<?endif?>
								<?if ($twitterLink):?>
                                <li><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_twitter.php"), false);?></li>
								<?endif?>
								<?if ($googlePlusLink):?>
                                <li><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_google.php"), false);?></li>
								<?endif?>
								<?if ($twitterLink):?>
                                <li><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_linkedin.php"), false);?></li>
								<?endif?>
								<?if ($rssLink):?>
                                <li><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_rss.php"), false);?></li>
								<?endif?>
                                </ul>									
                            </div>    
                            <div class="top-info-contact pull-right col-md-6">
							<?=GetMessage("TMPL_CONTACT")?>
							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/kontakt_tel_header.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
							 |    
							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/kontakt_mail_header.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
								 <div id="search" class="fa fa-search search-head"></div>
                            </div>                            
                  
                        </div>
                    </div><!--Topbar-info-close-->
			
			
			<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu", 
	array(
		"COMPONENT_TEMPLATE" => "top_menu",
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_THEME" => "site"
	),
	false
);?>
                                <div class="hide-mid navbar-collapse option-drop collapse in" id="bs-example-navbar-collapse-2" style="height: auto;">
                                  
                                  
                                  <ul class="nav navbar-nav navbar-right other-op">
                                    <li><i class="icon-phone2"></i>							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/kontakt_tel_header.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?></li>
                                    <li><i class="icon-mail"></i>							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/kontakt_mail_header.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?></li>
                                    
                                    <li><i class="icon-globe"></i>
								<?if ($facebookLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_facebook.php"), false);?>
								<?endif?>
								<?if ($twitterLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_twitter.php"), false);?>
								<?endif?>
								<?if ($googlePlusLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_google.php"), false);?>
								<?endif?>
								<?if ($twitterLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_linkedin.php"), false);?>
								<?endif?>
                                    </li>
                                    <li><i class="icon-search"></i>
									<?$APPLICATION->IncludeComponent(
									"bitrix:search.form",
									"search_2",
									Array(
										"COMPONENT_TEMPLATE" => ".default",
										"PAGE" => "#SITE_DIR#search/index.php",
										"USE_SUGGEST" => "N"
									)
									);?>
                                    </li>
                                    
                                  </ul>
                                </div><!-- /.navbar-collapse -->			
              </div>
            </header>
			            <div class="complete-content <?if($curPage != SITE_DIR."index.php"){?> content-footer-space <?}?>">
						<?if($curPage != SITE_DIR."index.php"){?>
						<div class="about-intro-wrap pull-left">
						     <div class="bread-crumb-wrap ibc-wrap-<?=rand('1', '6')?>">
								<div class="container">
							<!--Title / Beadcrumb-->
									<div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
										<div class="bread-heading"><h1><?=$APPLICATION->ShowTitle(false);?></h1></div>
										
											<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "med", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"START_FROM" => "0",	// ����� ������, ������� � �������� ����� ��������� ������������� �������
		"PATH" => "",	// ����, ��� �������� ����� ��������� ������������� ������� (�� ���������, ������� ����)
		"SITE_ID" => "med_bit_ecommerce",	// C��� (��������������� � ������ ������������� ������, ����� DOCUMENT_ROOT � ������ ������)
	),
	false
);?>
									
									</div>
								 </div>
							 </div>
         <div class="container">
          <div class="row">            
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 dept-tabs-wrap wow animated">			
			<div class="tabbable tabs-left">
						<?}?>
						
						