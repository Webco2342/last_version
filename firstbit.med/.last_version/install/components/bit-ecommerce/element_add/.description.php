<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MF_NAME"),
	"DESCRIPTION" => '',
	"ICON" => "/images/sds-icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "bit-ecommerce",
	),
);

?>