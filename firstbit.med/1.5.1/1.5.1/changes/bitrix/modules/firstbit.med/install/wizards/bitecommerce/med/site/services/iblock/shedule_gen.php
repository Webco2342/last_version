<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;


COption::SetOptionString($moduleId, "SEF_FOLDER", WIZARD_SITE_DIR);

if (WIZARD_INSTALL_DEMO_DATA) {
	CModule::IncludeModule('firstbit.sheduler');
	CBitSheduler::CleanNGenerate();

	CAgent::AddAgent("CBitSheduler::CleanNGenerate();", 
	"firstbit.sheduler", 
	"N", 
	86400, 
	date('d.m.Y 00:00:00', strtotime(date('d.m.Y', time() + 86400))), 
	"Y", 
	date('d.m.Y 00:00:00', strtotime(date('d.m.Y', time()))),
	30);
}
