<?

class CBitSheduler {


	static $DaysToShedule = 14;

	static function CleanNGenerate($DaysToShedule = 14) {

		if (!CModule::IncludeModule('iblock')) {
			return;
		}

		$DOCTOR_IBLOCK_ID = COption::GetOptionInt('firstbit.sheduler','DOCTOR_IBLOCK_ID');
		$SHEDULE_IBLOCK_ID = COption::GetOptionInt('firstbit.sheduler','SHEDULE_IBLOCK_ID');
		$CLINIC_IBLOCK_ID = COption::GetOptionInt('firstbit.sheduler','CLINIC_IBLOCK_ID');
		

		if (!$DaysToShedule) {
			$DaysToShedule = self::$DaysToShedule;
		}
		global $DB;
		////////////////// КЛИНИКИ ..................................
		//@f:off
		$obClinics = new CIBlockElement();
		$dbClinics = $obClinics -> GetList(
											array(),
											array(
													'IBLOCK_ID' => $CLINIC_IBLOCK_ID,
													'ACTIVE' => ''
												),
											false,
											false,
											array(
													"ID",
													'NAME',
													'IBLOCK_ID',
													'ACTIVE'
												)
											);

		$arCLINICS = array();
		while ($arClinic = $dbClinics -> Fetch()) {
			$arCLINICS[$arClinic['ID']] = $arClinic;
		}
		
		// ............ в расписании

		$obSheduleSection = new CIBlockSection();

		$dbSheduleClinics = $obSheduleSection -> GetList(
														array(),
														array(
																'IBLOCK_ID' => $SHEDULE_IBLOCK_ID,
																'ACTIVE' => '',
															//	'!UF_IB_CLINIC' => false, //@TODO that`s weird...
																'DEPTH_LEVEL' => 1
															),
														false,
														array(
																'ID',
																'IBLOCK_ID',
																'NAME',
																'UF_IB_CLINIC',
																'ACTIVE'
															)
														);

		$arSheduleCLINICS = array();
		while ($arSheduleClinic = $dbSheduleClinics -> Fetch()) { 
			$arSheduleCLINICS[$arSheduleClinic['UF_IB_CLINIC']] = $arSheduleClinic;
		}
			
		$arAddNewClinicIDs = array_diff(array_keys($arCLINICS), array_keys($arSheduleCLINICS));
		$arCLI2SHECLI=array();
		if (count($arAddNewClinicIDs)) {
			foreach ($arAddNewClinicIDs as $intClinicToLoadID) {
					
				if (
					$newShedileClinicSectionID = 	$obSheduleSection
													-> Add(
															$arrLoadNewSheduleClinic =
															array(
																	'IBLOCK_ID' => $SHEDULE_IBLOCK_ID,
																	'IBLOCK_SECTION_ID' => false,
																	'NAME' => $arCLINICS[$intClinicToLoadID]['NAME'],
																	'ACTIVE' => $arCLINICS[$intClinicToLoadID]['ACTIVE'],
																	'UF_IB_CLINIC' => $intClinicToLoadID
																)
														)
					)
				{
					$arrLoadNewSheduleClinic['ID']=$newShedileClinicSectionID;
					$arSheduleCLINICS[$intClinicToLoadID]=$arrLoadNewSheduleClinic;
					$arCLI2SHECLI[$intClinicToLoadID]=$arrLoadNewSheduleClinic['ID'];
				} else {
					echo $obSheduleSection->LAST_ERROR;
				}
			}
		}
		
		$arDeleteSheduleClinicsIDs=array_diff(array_keys($arSheduleCLINICS), array_keys($arCLINICS));
	
		if(count($arDeleteSheduleClinicsIDs)) {
			foreach($arDeleteSheduleClinicsIDs as $intDeleteClinicID) {
				$obSheduleSection->Update(
											$arSheduleCLINICS[$intDeleteClinicID]['ID'],
											array(
													'NAME'=>'Удалена - '.str_replace('Удалена - ','',$arSheduleCLINICS[$intDeleteClinicID]['NAME']), //@TODO sorry for this. That`s chrome make 4 requests. may be innodb can help. 
													'ACTIVE'=>'N',
													'UF_IB_CLINIC'=>false
												)
										);
				unset($arSheduleCLINICS[$intDeleteClinicID]);
			}
		}
	
	
	
		foreach($arSheduleCLINICS as $intSheduleClinicID=>$arSheduleClinicData) {
			if	(
				$arCLINICS[$intSheduleClinicID]
				&&	(
						$arSheduleClinicData['ACTIVE']!=$arCLINICS[$intSheduleClinicID]['ACTIVE']
					||	$arSheduleClinicData['NAME']!=$arCLINICS[$intSheduleClinicID]['NAME']
					)
				) { 
					$obSheduleSection->Update(
											$arSheduleClinicData['ID'],
											$arUpdateData=array(
													'NAME'=>$arCLINICS[$intSheduleClinicID]['NAME'],
													'ACTIVE'=>$arCLINICS[$intSheduleClinicID]['ACTIVE']
												)
											);
					if($arUpdateData['ACTIVE']=='N') {
						unset($arSheduleCLINICS[$intSheduleClinicID]);
					} else {
						$arCLI2SHECLI[$arSheduleClinicData['UF_IB_CLINIC']]=$arSheduleClinicData['ID'];
					}
				} else {
					$arCLI2SHECLI[$arSheduleClinicData['UF_IB_CLINIC']]=$arSheduleClinicData['ID'];
				}
		}
		
		$arSHECLI2CLI=array_flip($arCLI2SHECLI);
		
		////////////////// ВРАЧИ ..................................
		
		// ..... свойство "рабочие дни"
		$dbWorkDays = CIBlockProperty::GetPropertyEnum(
														"L_WORK_DAYS",
														Array(),
														Array(
																"IBLOCK_ID"=>$DOCTOR_IBLOCK_ID
															)
													);
		$arDAYXML2ID=array();
		$arDAYXML2TEXT=array();
		while($arWorkDay=$dbWorkDays->Fetch()) {
			$arDAYXML2ID[$arWorkDay['ID']]=str_replace('D_','',$arWorkDay['XML_ID']);
			$arDAYXML2TEXT[$arDAYXML2ID[$arWorkDay['ID']]]=$arWorkDay['VALUE'];
		}
		
		// ..... сами врачи
		$obDoctor=new CIBlockElement();
		
		$dbDoctors=$obDoctor->GetList(
										array(),
										array(
												'IBLOCK_ID'=>$DOCTOR_IBLOCK_ID,
												'ACTIVE'=>'',
												'!PROPERTY_IB_CLINIC'=>false	
											),
										false,
										false,
										array(
											'ID',
											'IBLOCK_ID',
											'ACTIVE',
											'NAME',
											'PROPERTY_IB_CLINIC'
										)
									);
		$arDOCTORS=array();
		$arDOC2CLI=array();
		
		while($arObDoctor=$dbDoctors->GetNextElement()) {
			$arDoctor=array_merge($arObDoctor->GetFields(),$arObDoctor->GetProperties());
			
			$arDoctor['DAYS']=array();
			
			$arDoctor['PROPERTY_IB_CLINIC_VALUE']	=	$arDoctor['IB_CLINIC']['VALUE'];
			$arDoctor['PROPERTY_INT_START_DAY_VALUE']		=	$arDoctor['INT_START_DAY']['VALUE'];
			$arDoctor['PROPERTY_INT_STOP_DAY_VALUE']		=	$arDoctor['INT_STOP_DAY']['VALUE'];
			$arDoctor['PROPERTY_INT_DURATION_VALUE']		=	$arDoctor['INT_DURATION']['VALUE'];
			
			
			foreach($arDoctor['L_WORK_DAYS']['VALUE_ENUM_ID'] as $intWorkDayENUM) {
				if(array_key_exists($intWorkDayENUM, $arDAYXML2ID)){
					$arDoctor['DAYS'][$arDAYXML2ID[$intWorkDayENUM]]=true;
				}
			}

			$arDOCTORS[$arDoctor['ID']]=$arDoctor;
		}
	
		// ............ в расписании
		
		$obSheduledDoctors=new CIBlockSection();
		$dbSheduleDoctors=$obSheduledDoctors->GetList(
													array(),
													array(
															'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
															'DEPTH_LEVEL'=>2,
															'ACTIVE'=>''
														),
													false,
													array(
															'ID',
															'ACTIVE',
															'NAME',
															'UF_IB_DOCTOR',
															'IBLOCK_SECTION_ID'
													)
													
		);
		
		$arSheduleDOCTORS=array();
		while($arSheduleDoctor=$dbSheduleDoctors->Fetch()) {
			$arSheduleDoctor['CLINIC']=$arSHECLI2CLI[$arSheduleDoctor['IBLOCK_SECTION_ID']];
			$arSheduleDOCTORS[$arSheduleDoctor['UF_IB_DOCTOR']]=$arSheduleDoctor;
		}
	
		/// ........... создаем новых ........
		
		$arAddNewDoctorsIDs=array_diff(array_keys($arDOCTORS), array_keys($arSheduleDOCTORS));
		$arSheduleDoctorsIDs=array();		
		foreach($arAddNewDoctorsIDs as $intRealDocID) {
			if($newDocId=
			$obSheduledDoctors->Add(
						$arDocFields=array(
										'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
										'NAME'=>$arDOCTORS[$intRealDocID]['NAME'],
										'IBLOCK_SECTION_ID'=>$arCLI2SHECLI[$arDOCTORS[$intRealDocID]['PROPERTY_IB_CLINIC_VALUE']],
										'ACTIVE'=>$arDOCTORS[$intRealDocID]['ACTIVE'],
										'UF_IB_DOCTOR'=>$intRealDocID
										)
									)
			) {
				$arDocFields['CLINIC']=$arDOCTORS[$intRealDocID]['PROPERTY_IB_CLINIC_VALUE'];
				$arDocFields['ID']=$newDocId;
				$arSheduleDOCTORS[$arDocFields['UF_IB_DOCTOR']]=$arDocFields;
				$arSheduleDoctorsIDs[$newDocId]=$intRealDocID;
			} else {
				echo $obSheduledDoctors->LAST_ERROR;
			}
		}		
				
		//// ............... проверяем изменения .............................
		
		foreach($arSheduleDOCTORS as $intRealDoctorID=>$arSheduleDoctorData) {
			
			if($arSheduleDoctorData['CLINIC']!=$arDOCTORS[$intRealDoctorID]['PROPERTY_IB_CLINIC_VALUE']) {
				$obSheduledDoctors->Update(
											$arSheduleDoctorData['ID'],
											array(
													'IBLOCK_SECTION_ID'=>$arCLI2SHECLI[$arDOCTORS[$intRealDoctorID]['PROPERTY_IB_CLINIC_VALUE']]
												)
											);
			}
			
			if($arDOCTORS[$intRealDoctorID]['ACTIVE']!=$arSheduleDoctorData['ACTIVE']) {
				$obSheduledDoctors->Update(
											$arSheduleDoctorData['ID'],
											array(
													'ACTIVE'=>$arDOCTORS[$intRealDoctorID]['ACTIVE']
												)
											);
			}
			
			if(!isset($arDOCTORS[$intRealDoctorID])) {
					$obSheduledDoctors->Update(
											$arSheduleDoctorData['ID'],
											array(
													'NAME'=>'Удален - '.str_replace('Удален - ','',$arSheduleDoctorData['NAME']), //@TODO sorry for this. That`s chrome make 4 requests. may be innodb can help. 
													'ACTIVE'=>'N'
												)
										);
				unset($arSheduleDOCTORS[$intRealDoctorID]);
			} else {
				$arSheduleDoctorsIDs[$arSheduleDoctorData['ID']]=$intRealDoctorID;
			}
			
		}

		///////// добавляем рабочие дни на ближайшую неделю.....
		
		// ..... свойство "свободно"
		$dbFreeProp = CIBlockProperty::GetPropertyEnum(
														"B_FREE",
														Array(),
														Array(
																"IBLOCK_ID"=>$SHEDULE_IBLOCK_ID
															)
													);
		$arFree2ID=array();
		while($arFreeProp=$dbFreeProp->Fetch()) {
			$arFree2ID[$arFreeProp['XML_ID']]=$arFreeProp['ID'];
		}
	
		
	
		//.... выбираем уже имеющиеся
		$obDays=new CIBlockSection();
		
		$dbDays=$obDays->GetList(
								array(),
								array(
										'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
										'DEPTH_LEVEL'=>3,
										'ACTIVE'=>'',
										'SECTION_ID'=>array_keys($arSheduleDoctorsIDs),
										'>=UF_D_DATE'=>time()
									),
								false,
								array(
										'ID',
										'IBLOCK_ID',
										'ACTIVE',
										'NAME',
										'IBLOCK_SECTION_ID',
										'UF_D_DATE'
									)
								);
		
		$arDAYS=array();
		$arDAYSIDs=array();
		$arDAYSID2DATE=array();
		while($arDAY=$dbDays->Fetch()) {
			$arDAYS[$arDAY['IBLOCK_SECTION_ID']][$arDAY['UF_D_DATE']]=$arDAY;
			$arDAYSIDs[]=$arDAY['ID'];
			$arDAYSID2DATE[$arDAY['ID']]=$arDAY['UF_D_DATE'];
		}
		$today=strtotime(date('d.m.Y'));
		
		$arDoctorSDayToCreate=array();
		
		foreach($arSheduleDoctorsIDs as $intSheduleDoctorID=>$intRealDoctorID) {
			list($StartH,$StartM)=explode('.',$arDOCTORS[$intRealDoctorID]['PROPERTY_INT_START_DAY_VALUE']);
			$StartTime=$StartH*60+str_pad($StartM,2,'0',STR_PAD_RIGHT);
			
			list($StopH,$StopM)=explode('.',$arDOCTORS[$intRealDoctorID]['PROPERTY_INT_STOP_DAY_VALUE']);
			$StopTime=$StopH*60+str_pad($StopM,2,'0',STR_PAD_RIGHT);
			
			$Duration=$arDOCTORS[$intRealDoctorID]['PROPERTY_INT_DURATION_VALUE'];
			
			for($curDay=1; $curDay<=$DaysToShedule; $curDay++) {
				$DaY=$today+86400*$curDay;
				if(
						array_key_exists($WeekDay=date('w',$DaY), $arDOCTORS[$intRealDoctorID]['DAYS'])		//Doctor exist in this day
					&&	(											
								!isset($arDAYS[$intSheduleDoctorID]) 		//and all days NOT created 
							|| 	!isset($arDAYS[$intSheduleDoctorID][$DaY])	//current DAY
						)
				) {
					if($newDayID=$obDays->Add(
									$arNewDay=array(
													'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
													'IBLOCK_SECTION_ID'=>$intSheduleDoctorID,
													'NAME'=>date('d.m.Y',$DaY).' - '.$arDAYXML2TEXT[$WeekDay],
													'ACTIVE'=>'Y',
													'UF_D_DATE'=>$DaY
													)
											)
					) {
						$arNewDay['ID']=$newDayID;
						$arDAYS[$intSheduleDoctorID][$DaY]=$arNewDay;
					} else {
						echo $obDays->LAST_ERROR;
						continue;
					}
				}
				
				//DAY exists or just created
				
				for($CurTime=$StartTime;$CurTime<$StopTime;$CurTime=$CurTime+$Duration) {
					$dayTimeTS=$DaY+$CurTime*60;
					$arDoctorSDayToCreate[$intSheduleDoctorID][$DaY][date('H:i',$dayTimeTS)]=$dayTimeTS;
				}
				
			}
			
		}
		
		$obExistDayRows= new CIBlockElement;
		
		foreach($arDAYS as $intSheduleDocId=>$arDoctorDays) { 
			foreach($arDoctorDays as $intDayTS=>$arDay) { 
				
				$dbExistDayRows=$obExistDayRows->GetList(
															array(),
															array(
																	'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
																	'ACTIVE'=>'',
																	'SECTION_ID'=>$arDay['ID']
																),
															false,
															false,
															array(
																	'ID',
																	'IBLOCK_ID',
																	'NAME',
																	'IBLOCK_SECTION_ID',
																	'ACTIVE'
																)	
														);
				
				$arEXISTROWSInDAY=array();
				while($arExistRow=$dbExistDayRows->Fetch()) {
					$arEXISTROWSInDAY[$arExistRow['NAME']]=$arExistRow;
				}
			
				$arCreateDays=array_diff(
											array_keys($arDoctorSDayToCreate[$intSheduleDocId][$intDayTS]),
											array_keys($arEXISTROWSInDAY)
										);
			
				foreach($arCreateDays as $strCreateDateName) {
					if(
						$newDay=$obExistDayRows->Add(
														$C=array(
															'NAME'=>$strCreateDateName,
															'IBLOCK_ID'=>$SHEDULE_IBLOCK_ID,
															'IBLOCK_SECTION_ID'=>$arDAYS[$intSheduleDocId][$intDayTS]['ID'], 
															'ACTIVE'=>'Y',
															'PROPERTY_VALUES'=>array(
																					'B_FREE'=>$arFree2ID['Y'],
																					'TM_TIME'=>$arDoctorSDayToCreate[$intSheduleDocId][$intDayTS][$strCreateDateName] // i knew, that`s really awesome
																					)
														)
													)
					) {
					} else {
						echo $obExistDayRows->LAST_ERROR;
					}
					
				}
		
			}
		}
		
		// удаляем старые данные
		
		$dbOldSheduleRows=$obExistDayRows->GetList(
													array('ID'=>'ASC'),
													array(
														'IBLOCK_ID'			=>	$SHEDULE_IBLOCK_ID,
														'<PROPERTY_TM_TIME'	=>	$today,
														'PROPERTY_B_FREE'	=>	$arFree2ID['Y']
													),
													false,
													false,
													array(
															'ID',
															'IBLOCK_ID',
															'NAME'
														)
												);
		
		while($arOldRow=$dbOldSheduleRows->Fetch()){
			$obExistDayRows->Delete($arOldRow['ID']);
		}
		
		$dbEmptyDays=$obDays->GetList(
										array(),
										array(
												'IBLOCK_ID'		=>	$SHEDULE_IBLOCK_ID,
												'<=UF_D_DATE'	=>	$today,
												'DEPTH_LEVEL'	=>	3
											),
										true
									);
		
		while($arOldDay=$dbEmptyDays->Fetch()) {
			if($arOldDay['ELEMENT_CNT']==0){
				$obDays->Delete($arOldDay['ID']);
			}
		}
		
		return 'CBitSheduler::CleanNGenerate('.$DaysToShedule.');';
	}

}
