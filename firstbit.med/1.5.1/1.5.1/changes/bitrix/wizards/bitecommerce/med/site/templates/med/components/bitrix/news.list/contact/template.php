<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$frame = $this->createFrame("contact", false)->begin(); 
$CurrentItem=array();
?>

<script src="http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU" type="text/javascript"></script>
 <div id="contact-version-two">
					<script> 
					var myMap;
						ymaps.ready(init);
						function init () {
							myMap = new ymaps.Map('map-canvas', {
						<?foreach($arResult["ITEMS"] as $key=>$arItems):
							if($_REQUEST['ELEMENT_ID']>0){
							if($_REQUEST['ELEMENT_ID']==$arItems['ID']){
							$CurrentItem=$arItems;
							?>	
								center: [<?=$arItems['PROPERTIES']['MAPS']['VALUE']?>], 
							<?}							
							}else{ 
							if($key==0){
								$CurrentItem=$arItems;
							?>	
								center: [<?=$arItems['PROPERTIES']['MAPS']['VALUE']?>], 
							<?}
							}
						endforeach;?>
								zoom: 11
							});
						myGeoObject = new ymaps.GeoObject({}, {});
						myMap.geoObjects
						.add(myGeoObject)
						<?foreach($arResult["ITEMS"] as $key=>$arItem):
							if($_REQUEST['ELEMENT_ID']>0){
							if($_REQUEST['ELEMENT_ID']!=$arItem['ID']){							
							?>		
								 .add(new ymaps.Placemark([<?=$arItem['PROPERTIES']['MAPS']['VALUE']?>], {
									hintContent: '<?=$arItem['NAME']?>',
									locationUrl: '<?=SITE_DIR?>contact/<?=$arItem["ID"]?>/'
								}, {
									preset: 'islands#icon',
									iconColor: '#0095b6'
								}))
							<?
							}
							}else{
							if($key!=0){
							?>	
								 .add(new ymaps.Placemark([<?=$arItem['PROPERTIES']['MAPS']['VALUE']?>], {
									hintContent: '<?=$arItem['NAME']?>',
									locationUrl: '<?=SITE_DIR?>contact/<?=$arItem["ID"]?>/'
								}, {
									preset: 'islands#icon',
									iconColor: '#0095b6'
								}))
							<?}
							}
						endforeach;?>								
							myMap.behaviors.disable('scrollZoom'); 
							var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
							hintContent: '<?=$CurrentItem['NAME']?>',
							locationUrl: '<?=SITE_DIR?>contact/<?=$CurrentItem["ID"]?>/'			
							}, {
								iconLayout: 'default#image',
								iconImageHref: '<?=SITE_TEMPLATE_PATH?>/images/location.png',
								iconImageSize: [83, 127],
								iconImageOffset: [-40, -110]
							}, {
							preset: 'islands#redDotIcon'
						});
						myMap.geoObjects.add(myPlacemark);
						myMap.geoObjects.events.add('click', function (e) {
							var target = e.get('target');        
							window.location.href = target.properties.get('locationUrl');
						});
						}	
					</script>
					
				<div class="container">
					<h1 class='dept-title-tabs clinic_title'><?=$CurrentItem['NAME']?></h1>
            	<div class="pull-left map-full no-pad contact-v1-map">
                	<div id="map-canvas"></div>
                	<div class="map-shadow" ></div>
                </div>
				</div><!--container end-->	
						<?foreach($arResult["ITEMS"] as $key=>$arItems):
							if($_REQUEST['ELEMENT_ID']>0){
							if($_REQUEST['ELEMENT_ID']==$arItems['ID']){
							?>					
		<div class="container">
         <div class="mid-widgets-serices  text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad top-pad pull-left services-page">
         
            <!--service box-->
			<?if($arItems['PROPERTIES']['ADRES']['VALUE']){?>
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-map-marker panel-icon"></i></div><div class="service-heading"><?=GetMessage("ADRES")?></div></div>
                <p><?=$arItems['PROPERTIES']['ADRES']['VALUE']?></p>
              
            </div>
            <?}?>
			
			<?if($arItems['PROPERTIES']['PHONE']['VALUE']){?>
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-phone panel-icon"></i></div><div class="service-heading"><?=GetMessage("TEL")?></div></div>
                <p><?=$arItems['PROPERTIES']['PHONE']['VALUE']?></p>
                
            </div>
			<?}?>
			
			<?if($arItems['PROPERTIES']['EMAIL']['VALUE']){?>            
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-envelope-o panel-icon"></i></div><div class="service-heading">Email</div></div>
                <p><?=$arItems['PROPERTIES']['EMAIL']['VALUE']?></p>
               
            </div>
 			<?}?>
			
			<?if($arItems['PROPERTIES']['SOC']['VALUE']){?> 			
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-facebook panel-icon"></i></div><div class="service-heading"><?=GetMessage("SOC")?></div></div>
                <p><?=$arItems['PROPERTIES']['SOC']['VALUE']?></p>
              
            </div>
 			<?}?>
			
			</div>  <!--sservice box end--> 
		 </div><!--container-->  		
						<?}							
							}else{
							if($key==0){
							?>	
				<div class="container">
       <div class="mid-widgets-serices  text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 no-pad top-pad pull-left services-page">
         
              <!--service box-->
			<?if($arItems['PROPERTIES']['ADRES']['VALUE']){?>
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-map-marker panel-icon"></i></div><div class="service-heading"><?=GetMessage("ADRES")?></div></div>
                <p><?=$arItems['PROPERTIES']['ADRES']['VALUE']?></p>
              
            </div>
            <?}?>
			
			<?if($arItems['PROPERTIES']['PHONE']['VALUE']){?>
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-phone panel-icon"></i></div><div class="service-heading"><?=GetMessage("TEL")?></div></div>
                <p><?=$arItems['PROPERTIES']['PHONE']['VALUE']?></p>
                
            </div>
			<?}?>
			
			<?if($arItems['PROPERTIES']['EMAIL']['VALUE']){?>            
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-envelope-o panel-icon"></i></div><div class="service-heading">Email</div></div>
                <p><?=$arItems['PROPERTIES']['EMAIL']['VALUE']?></p>
               
            </div>
 			<?}?>
			
			<?if($arItems['PROPERTIES']['SOC']['VALUE']){?> 			
            <!--service box-->
            <div class="col-sm-5 col-xs-12 col-md-3 col-lg-3 service-box no-pad wow animated">
                <div class="service-title"><div class="service-icon-container rot-y"><i class="fa fa-facebook panel-icon"></i></div><div class="service-heading"><?=GetMessage("SOC")?></div></div>
                <p><?=$arItems['PROPERTIES']['SOC']['VALUE']?></p>
              
            </div>
 			<?}?>
			
			</div>  <!--sservice box end--> 
		 </div><!--container-->  	
		 
			<?}
							}endforeach;?>		 
		<div class="container" style="padding-bottom: 50px;">
             <div class="row">
			 
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3" >                    
                   
					<div id="imedica-dep-accordion">
				<?
				$len = sizeof($arResult["ITEMS"]); 
				foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					--$len;
					
					?>




						    <h3 class="<?if(!$len) {?>last-child-ac ilast-child-acc<?}?>"><i class="<?=$arItem['PROPERTIES']['CODE_ICO']['VALUE']?> dept-icon"></i><span class="dep-txt"><?=$arItem['NAME_IMPLODE']?></span></h3>
                            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                
                                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="img-responsive dept-author-img-desk col-md-4" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
                                <div class="dept-content pull-left col-md-7 col-lg-8" >
                              
                                <p><?=$arItem['PREVIEW_TEXT_IMPLODE']?></p>
                                
                                
                                <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="dept-details-butt"><?echo GetMessage("CT_BNL_GOTO_DETAIL")?></a>
                                <div class="purchase-strip-blue dept-apponit-butt"><div class="color-4">
                                    <p class="ipurchase-paragraph">
                                        <a href="<?=SITE_DIR?>contact/<?echo $arItem["ID"]?>/"><button class="icon-calendar btn btn-4 btn-4a notViewed"><?=GetMessage("CONTACT")?></button></a>
                                    </p>
                                </div></div>
                                
                                <div class="vspacer"></div>
                                </div>
                            </div>


				<?
					
				endforeach;?>
					</div>
				</div>			 
			 
			 </div>
		</div>
         <div class="container">
			<div class="row">	
			<div class="subtitle col-xs-12 col-sm-12 col-md-6 col-md-offset-3 pull-left news-sub icontact-widg"><center><?=GetMessage("TITLE")?></center></div>
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.feedback_2", 
	"contact_form", 
	array(
		"COMPONENT_TEMPLATE" => "contact_form",
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => GetMessage("SPAS"),
		"EMAIL_TO" => $arItems["PROPERTIES"]["EMAIL"]["VALUE"],
		"REQUIRED_FIELDS" => array(
		),
		"EVENT_MESSAGE_ID" => array(
			0 => $arParams["EMAIL"],
		)
	),
	$component
);?>
			</div>
		</div>

   </div>
<?$frame->end(); ?>