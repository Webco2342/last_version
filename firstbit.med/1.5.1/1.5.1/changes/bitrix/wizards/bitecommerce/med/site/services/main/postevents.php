<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


	// Настройки для формы обратной связи
	// Проверим, есть ли нужное нам событие FEEDBACK_FORM_MAIN
	$rs = CEventType::getList(array('EVENT_NAME' => 'FEEDBACK_FORM_MAIN'));
	if( !$ar = $rs->fetch() )
	{
		$eventFields = array();
		$eventFields['ru'] = array(
			'LID' => 'ru',
			'EVENT_NAME' => 'FEEDBACK_FORM_MAIN',
			'NAME' => 'Отправка сообщения через форму обратной связи',
			'DESCRIPTION' => "#AUTHOR# - Автор сообщения \n#AUTHOR_EMAIL# - Email автора сообщения \n#TEXT# - Текст сообщения \n#EMAIL_FROM# - Email отправителя письма \n#EMAIL_TO# - Email получателя письма \n#TEL# - Телефон автора \n#TEXT_2# - Дата 1 \n#TEXT_2# - Дата 2",
			'SORT' => 100,
		);
		// ДОбавляем новый тип почтового события
		$et = new CEventType;
		$et->add($eventFields['ru']);
	}
	// В любом случае у нас теперь есть почтовое событие. Проверим наличие шаблона к нему
	// Есть ли привязанный к текущему сайту шаблон
	$rs = CEventMessage::getList($by, $order, array('TYPE_ID' => 'FEEDBACK_FORM_MAIN', 'SITE_ID' => $siteID));
	// Если шаблон не найден, то поищем шаблон, привязанный к другому сайту
	if( !$ar = $rs->fetch() )
	{
		$rs = CEventMessage::getList($by, $order, array('TYPE_ID' => 'FEEDBACK_FORM_MAIN'));
		// Если нет такого шаблона - создаем
		if( !$ar = $rs->fetch() )
		{
			$eventMessageFields = array(
				'LID' => $siteID,
				'EVENT_NAME' => 'FEEDBACK_FORM_MAIN',
				'ACTIVE' => 'Y',
				'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
				'EMAIL_TO' => '#EMAIL_TO#',
				'SUBJECT' => '#SITE_NAME#: Сообщение из формы обратной связи',
				'MESSAGE' => "Информационное сообщение сайта #SITE_NAME# \n------------------------------------------ \n \nВам было отправлено сообщение через форму обратной связи \n \nАвтор: #AUTHOR# \nE-mail автора: #AUTHOR_EMAIL# \nТелефон автора: #TEL# \nТекст сообщения: \n#TEXT# \n \nСообщение сгенерировано автоматически.",
				'BODY_TYPE' => 'text',
			);
			$em = new CEventMessage;			
			$FORM_ID_1 = $em->add($eventMessageFields);
			
			
			$eventMessageFields2 = array(
				'LID' => $siteID,
				'EVENT_NAME' => 'FEEDBACK_FORM_MAIN',
				'ACTIVE' => 'Y',
				'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
				'EMAIL_TO' => '#EMAIL_TO#',
				'SUBJECT' => '#SITE_NAME#: Запись на прием',
				'MESSAGE' => "Информационное сообщение сайта #SITE_NAME# \n------------------------------------------ \n \nВам было отправлено сообщение через форму обратной связи \n \nАвтор: #AUTHOR# \nE-mail автора: #AUTHOR_EMAIL# \nТелефон автора: #TEL# \nТекст сообщения: \n#TEXT# \nДата и удобное время: \n#TEXT_2#
				\n \nСообщение сгенерировано автоматически.",
				'BODY_TYPE' => 'text',
			);
			$em1 = new CEventMessage;
			$FORM_ID_2 = $em1->add($eventMessageFields2);

			
			$eventMessageFields3 = array(
				'LID' => $siteID,
				'EVENT_NAME' => 'FEEDBACK_FORM_MAIN',
				'ACTIVE' => 'Y',
				'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
				'EMAIL_TO' => '#EMAIL_TO#',
				'SUBJECT' => '#SITE_NAME#: Запись на госпитализацию',
				'MESSAGE' => "Информационное сообщение сайта #SITE_NAME# \n------------------------------------------ \n \nВам было отправлено сообщение через форму обратной связи \n \nАвтор: #AUTHOR# \nE-mail автора: #AUTHOR_EMAIL# \nТелефон автора: #TEL# \nТекст сообщения: \n#TEXT# \nДата и удобное время: \n#TEXT_2# по #TEXT_3#
				\n \nСообщение сгенерировано автоматически.",
				'BODY_TYPE' => 'text',
			);
			$em2 = new CEventMessage;
			$FORM_ID_3 = $em2->update($ar['ID'], $eventMessageFields3);
			
		}
		// Если есть шаблон - то привязываем его к обоим сайтам
		else
		{
			$sites = array($ar['LID']);
			$sites[] = $siteID;
			$eventMessageFields = array('LID' => $sites);
			$em = new CEventMessage;
			$em->add($eventMessageFields);
		}
	}

			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/include/left_block_services_feedback.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/about/licenses/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/services/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/form.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/section/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/section/detail.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/promotions/detail.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/promotions/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/hospitalization/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/contact/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/news/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/news/detail.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/blogs/index.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/blogs/detail.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/detail.php", array("FORM_ID_1" => $FORM_ID_1));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/index.php", array("FORM_ID_2" => $FORM_ID_2));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/appointments/form.php", array("FORM_ID_2" => $FORM_ID_2));
			CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/patient/hospitalization/index.php", array("FORM_ID_3" => $FORM_ID_3));