<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;

// установим в настройках корневую папку для чпу
COption::SetOptionString($moduleId, "SEF_FOLDER", WIZARD_SITE_DIR);

if(WIZARD_INSTALL_DEMO_DATA)
{
	$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/"); 
	
	$handle = @opendir($path);
	if ($handle)
	{
		while ($file = readdir($handle))
		{
			if (in_array($file, array(".", "..")))
				continue; 
			
			CopyDirFiles(
				$path.$file,
				WIZARD_SITE_PATH."/".$file,
				$rewrite = true, 
				$recursive = true,
				$delete_after_copy = false
			);
		}

        if(CModule::IncludeModule("search"))
            CSearch::ReIndexAll(Array(WIZARD_SITE_ID, WIZARD_SITE_DIR));
	}

	WizardServices::PatchHtaccess(WIZARD_SITE_PATH);
    WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR));

	$arUrlRewrite = array(); 
	if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
	{
		include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
	}

	$arNewUrlRewrite = array(
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."departament/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."departament/index.php",
	),

array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."services/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."services/index.php",
	),	
	
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/(.*)/(.*)/(.*)#",
		"RULE" => "SECTION_ID=\$1&ELEMENT_ID=\$2",
		"PATH" => WIZARD_SITE_DIR."personal/detail.php",
	),
	
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."personal/(.*)/(.*)#",
		"RULE" => "SECTION_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."personal/index.php",
	),	

array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."news/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."news/detail.php",
	),

array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."contact/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."contact/index.php",
	),	
	
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."patient/section/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."patient/section/detail.php",
	),	
	

array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."patient/promotions/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."patient/promotions/detail.php",
	),	
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."blogs/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."blogs/detail.php",
	),	
array(
		"CONDITION" => "#^".WIZARD_SITE_DIR."detail/(.*)/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => WIZARD_SITE_DIR."detail.php",
	)		
	); 
	
	foreach ($arNewUrlRewrite as $arUrl)
	{
		if (!in_array($arUrl, $arUrlRewrite))
		{
			CUrlRewriter::Add($arUrl);
		}
	}
}

WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR));
?>