<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
 if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array( 

	"PARAMETERS" => array(
		"DOCTOR_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("DOCTOR_ID"),
			"TYPE" => "INT",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => '',
			"REFRESH" => "N"
		), 		
	),

);
