<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

CUtil::InitJSCore(array('popup', 'jquery'));

$arResult = array();
if (!CModule::IncludeModule("iblock") || !CModule::IncludeModule("firstbit.sheduler")) {
	return;
}

//$arParams['DOCTOR_IBLOCK_ID'] = 8;
//$arParams['CLINIC_IBLOCK_ID'] = 4;
//$arParams['REQUESTS_IBLOCK_ID'] = 16;
//$arParams['SHEDULE_IBLOCK_ID'] = 16;

if (!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 36000000;
}

$arParams['SHEDULE_INTERVAL'] = $_REQUEST['SHEDULE_INTERVAL'] ? (int)$_REQUEST['SHEDULE_INTERVAL'] : strtotime(date('d.m.Y'));
$arParams['SHEDULE_INTERVAL'] = $arParams['SHEDULE_INTERVAL'] < strtotime(date('d.m.Y')) ? strtotime(date('d.m.Y')) : $arParams['SHEDULE_INTERVAL'];
if ($arResult['CAN_LESS'] = $arParams['SHEDULE_INTERVAL'] != strtotime(date('d.m.Y'))) {
	$arResult['PREV_INTERVAL'] = $arParams['SHEDULE_INTERVAL'] - 86400 * 7 - 2;
}

$arParams['SHEDULE_TIME'] = $_REQUEST['SHEDULE_TIME'];

//@f:off
$arParams['DOCTOR_ID'] = $arParams['DOCTOR_ID']  ?
												$arParams['DOCTOR_ID']
											:	(
													$_REQUEST['DOCTOR_ID'] ?
																			(int)$_REQUEST['DOCTOR_ID']
																		:	$arParams['DOCTOR_ID']
												);  // ������ ������
//@f:on

$dbFreeProp = CIBlockProperty::GetPropertyEnum("B_FREE", Array(), Array("IBLOCK_ID" => $arParams['SHEDULE_IBLOCK_ID']));
$arFree2ID = array();
while ($arFreeProp = $dbFreeProp -> Fetch()) {
	$arFree2ID[$arFreeProp['XML_ID']] = $arFreeProp['ID'];
}

if ($arParams['DOCTOR_ID']) {

	//2 ���� ������ @f:off
	if (
			$arDoctor = CIBlockElement::GetList(
												array(),
												array(
														'IBLOCK_ID' => $arParams['DOCTOR_IBLOCK_ID'],
														'ID' => $arParams['DOCTOR_ID'],
														'ACTIVE'=>'Y'
													),
												false,
												false,
												array(
														'ID',
														'IBLOCK_ID',
														'ACTIVE',
														'NAME',
														'DETAIL_PAGE_URL',
														'PREVIEW_TEXT',
														'DETAIL_TEXT',
														'PREVIEW_PICTURE',
														'PROPERTY_IB_CLINIC',
														'PROPERTY_IB_CLINIC.NAME',
														'PROPERTY_IB_CLINIC.PREVIEW_PICTURE',
														'PROPERTY_IB_CLINIC.DETAIL_PAGE_URL',
														'PROPERTY_IB_CLINIC.PROPERTY_ADRES',
														'PROPERTY_IB_CLINIC.PROPERTY_PHONE',
														'PROPERTY_IB_CLINIC.PROPERTY_EMAIL',
														'PROPERTY_L_WORK_DAYS',
														'PROPERTY_INT_START_DAY',
														'PROPERTY_INT_STOP_DAY',
														'PROPERTY_INT_DURATION',
														'PROPERTY_STR_SPECIAL' // fix 57.7
													)
												) -> GetNext()
		) {
	//@f:on
		$arResult['DOCTOR'] = $arDoctor;
	}
}

if ($arParams['INNER'] != true) {
	$strTemplatePage = 'form';

	if ($arParams['SHEDULE_INTERVAL'] && $arParams['DOCTOR_ID'] && $arDoctor) {
		// 2.5 ������ ��������� �������
		//@f:off
		$obDoctorSection=new CIBlockSection;
		if($arDoctorSheduleSection=$obDoctorSection->GetList(
																array(),
																array(
																	'IBLOCK_ID'=>$arParams['SHEDULE_IBLOCK_ID'],
																	'UF_IB_DOCTOR'=>$arDoctor['ID']
																),
																false,
																array(
																		'ID',
																		'IBLOCK_ID',
																		'UF_IB_DOCTOR'
																	)	
																
													)->GetNext()
		) {
		
			$obDoctorDays=new CIBlockSection;
			$dbDoctorDays=$obDoctorDays->GetList(
													array('UF_D_DATE'=>'ASC'),
													$F=array(
															'IBLOCK_ID'		=>	$arParams['SHEDULE_IBLOCK_ID'],
															'SECTION_ID'	=>	$arDoctorSheduleSection['ID'],
															'DEPTH_LEVEL'	=>	3,
															'>UF_D_DATE'	=>	$arParams['SHEDULE_INTERVAL']-2,
															'<UF_D_DATE'	=>	$arParams['SHEDULE_INTERVAL']
																			+	7*86400
																			+	1,
															'ACTIVE'=>'Y'
															
													),
													false,
													array(
															'ID',
															'IBLOCK_ID',
															'NAME',
															'ACTIVE',
															'UF_D_DATE',
															'IBLOCK_SECTION_ID'
													)
											
												);
			
			//check if exist more days
			$arResult['CAN_MORE']=(bool) $obDoctorDays->GetList(
													array('UF_D_DATE'=>'ASC'),
													$F=array(
															'IBLOCK_ID'		=>	$arParams['SHEDULE_IBLOCK_ID'],
															'SECTION_ID'	=>	$arDoctorSheduleSection['ID'],
															'DEPTH_LEVEL'	=>	3,
															'>UF_D_DATE'	=>	$arResult['NEXT_INTERVAL']
																			=	$arParams['SHEDULE_INTERVAL']
																			+	8*86400
																				,
															'ACTIVE'=>'Y'
															
													),
													false,
													array(
															'ID',
															'IBLOCK_ID',
															'NAME',
															'ACTIVE',
															'UF_D_DATE',
															'IBLOCK_SECTION_ID'
													)
											
												)->GetNext();
			$intLimiter=0; //coz nTopCount makes me cry (((
			while($arDoctorDay=$dbDoctorDays->Fetch()) {
			$arResult['DOCTOR']['DAYS'][$arDoctorDay['ID']]=$arDoctorDay;
				if(++$intLimiter>6) {
					break;
				}
			}
			
			$obShedule=new CIBlockElement;
			$dbSheduleRows=$obShedule->GetList(
												array('PROPERTY_TM_TIME'=>"ASC"),
												array(
														'IBLOCK_ID'=>$arParams['SHEDULE_IBLOCK_ID'],
														'ACTIVE'=>'Y',
														'SECTION_ID'=>array_keys($arResult['DOCTOR']['DAYS']),
														'>PROPERTY_TM_TIME'=>time()
													),
												false,
												false,
												array(
														'ID',
														'IBLOCK_ID',
														'NAME',
														'IBLOCK_SECTION_ID',
														'PROPERTY_B_FREE',
														'PROPERTY_TM_TIME'
													)
												);
			$arSheduleTimeOK=false;
			
			while($arSheduleRow=$dbSheduleRows->Fetch()) {
				
				if($arParams['SHEDULE_TIME']==$arSheduleRow['ID'] && $arFree2ID['Y']==$arSheduleRow['PROPERTY_B_FREE_ENUM_ID']) {
					
					$arResult['SHEDULE_DAY']=$arResult['DOCTOR']['DAYS'][$arSheduleRow['IBLOCK_SECTION_ID']];
					$arResult['SHEDULE_TIME']=$arSheduleRow;
				
					$arSheduleTimeOK=true;
				}
				$arResult['DOCTOR']['DAYS'][$arSheduleRow['IBLOCK_SECTION_ID']]['TIME'][$arSheduleRow['ID']]=$arSheduleRow;
			}
			
			foreach($arResult['DOCTOR']['DAYS'] as $intDayId=>$arDayData) {
				if(count($arDayData['TIME'])==0) {
					unset($arResult['DOCTORE']['DAYS'][$intDayId]);
				}
			}
			
		//@f:on
		} else {
			//@TODO ��� ������ �������������� ������
		}
	}

	if ($arParams['SHEDULE_TIME'] && $arParams['DOCTOR_ID'] && $arSheduleTimeOK) {
		// 3.5 ������� ����� - �������� �����
		$strTemplatePage = 'final_form';
		if ($_REQUEST['MAKE_RECORD']) {
			//@f:off
		
			$arRequiredFields=array(
										'NAME'=>GetMessage('NAME_REQ'),
										'PHONE'=>GetMessage('PHONE_REQ'),
										'DAY_TIME'=>GetMessage('DAY_TIME_REQ')
									);
			//@f:on
			$arResult['ERRORS'] = array();
			foreach ($arRequiredFields as $strFieldCode => $strErrorMessage) {
				if (strlen($_REQUEST[$strFieldCode]) == 0) {
					$arResult['ERRORS'][] = $strErrorMessage;
				}
			}

			if (strlen($_REQUEST['EMAIL']) && !filter_var($_REQUEST['EMAIL'], FILTER_VALIDATE_EMAIL)) {
				$arResult['ERRORS'][] = GetMessage('ERR_EMAIL');
			}

			if (count($arResult['ERRORS']) == 0) {
				//@f:off
				$arAddFields=array(
									'IBLOCK_ID'=>$arParams['REQUESTS_IBLOCK_ID'],
									'NAME'=>			($Name=(string)$_REQUEST['NAME'])
												.' - '.	$arResult['DOCTOR']['NAME']
												.' - '.	$arResult['SHEDULE_DAY']['NAME']
												.' - '.	$arResult['SHEDULE_TIME']['NAME'],
									'ACTIVE'=>"Y",
									'IBLOCK_SECTION_ID'=>false,
									'PROPERTY_VALUES'=>array(
															'SH_F_NAME'		=>				$Name,
															'SH_F_PHONE'	=>	(string)	$_REQUEST['PHONE'],
															'SH_F_EMAIL'	=>	(string)	$_REQUEST['EMAIL'],
															'SH_F_COMMENT'	=>	(string)	$_REQUEST['COMMENT'],
															'SH_F_DOCTOR'	=>				$arResult['DOCTOR']['ID'],
															'SH_F_DATE'		=>				$arResult['SHEDULE_DAY']['ID'],
															'SH_F_TIME'		=>				$arResult['SHEDULE_TIME']['ID']
														)
								);	
				//@f:on
				$obRequest = new CIBlockElement();
				$arResult['OK'] = false;
				if ($newRequestID = $obRequest -> Add($arAddFields, false, false, true)) {
					$arResult['OK'] = true;
					$arResult['NEW_REQ_ID'] = $newRequestID;

					CIBlockElement::SetPropertyValueCode($arResult['SHEDULE_TIME']['ID'], "B_FREE", $arFree2ID['N']);
					CIBlockElement::SetPropertyValueCode($arResult['SHEDULE_TIME']['ID'], "IB_REQUEST", $newRequestID);

					$strTemplatePage = 'ok';
				


					$arEventFields = array(
						"ID"                => $newRequestID,
						"AUTHOR"            => $Name,
						"AUTHOR_EMAIL"      => $_REQUEST['EMAIL'],
						"EMAIL_FROM"        => $_REQUEST['EMAIL'],
						"TEL"        		=> $_REQUEST['PHONE'],						
						"TEXT"        		=> $_REQUEST['COMMENT'],	
						"DOCTOR"        	=> $arResult['DOCTOR']['NAME'],		
						"F_DATE"        	=> $arResult['SHEDULE_DAY']['NAME'],	
						"F_TIME"        	=> $arResult['SHEDULE_TIME']['NAME'],						
						);
					$arrSites = array();
					$objSites = CSite::GetList();
					while ($arrSite = $objSites->Fetch())
					$arrSites[] = $arrSite["ID"];
					CEvent::Send("ADD_SH", $arrSites, $arEventFields);

				
				
				} else {
					$arResult['ERRORS'][] = $obRequest -> LAST_ERROR;
				}
			}

		}

	}

	if (!is_array($arResult['DOCTOR'])) {
		// 1 ����� ����� � �������
		$obClinics = new CIBlockElement;
		//@f:off
		$dbClinics = $obClinics -> GetList(
											array('sort' => 'asc'),
											array(
													'IBLOCK_ID' => $arParams['CLINIC_IBLOCK_ID'],
													'ACTIVE' => 'Y'
												),
											false,
											false,
											array(
												'ID',
												'IBLOCK_ID',
												'NAME',
												'PREVIEW_TEXT',
												'PREVIEW_PICTURE',
												'DETAIL_PICTURE',
												'DETAIL_PAGE_URL'
											)
										);
		//@f"on"
		$arResult['CLINICS'] = array();
		while ($arClinic = $dbClinics -> GetNext()) {
			$arResult['CLINICS'][$arClinic['ID']] = $arClinic;
		}

		$obDoctors = new CIBlockElement;
		//@f:off
		$dbDoctors=$obDoctors->GetList(
										array('SORT'=>'ASC'),
										array(
												'IBLOCK_ID'=>$arParams['DOCTOR_IBLOCK_ID'],
												'ACTIVE'=>'Y',
												'PROPERTY_IB_CLINIC'		=>array_keys($arResult['CLINICS']),
												'!PROPERTY_L_WORK_DAYS'		=>false,
												'!PROPERTY_INT_START_DAY'	=>false,
												'!PROPERTY_INT_STOP_DAY'	=>false,
												'!PROPERTY_INT_DURATION'	=>false
											),
										false,
										false,
										array(
												'ID',
												'IBLOCK_ID',
												'ACTIVE',
												'NAME',
												'PREVIEW_TEXT',
												'DETAIL_TEXT',
												'DETAIL_PAGE_URL',
												'PREVIEW_PICTURE',
												'PROPERTY_IB_CLINIC',
												'PROPERTY_STR_SPECIAL' // fix 57.7
											)
											
									);
		//@f:on

		while ($arDoctor = $dbDoctors -> GetNext()) {
			$arResult['CLINICS'][$arDoctor['PROPERTY_IB_CLINIC_VALUE']]['DOCTORS'][$arDoctor['ID']] = $arDoctor;
		}

		foreach ($arResult['CLINICS'] as $intClinicID => $arClinic) {
			if (count($arClinic['DOCTORS']) == 0) {
				unset($arResult['CLINICS'][$intClinicID]);
			}
		}

		$strTemplatePage = 'clinics';

	}
}
$this -> IncludeComponentTemplate($strTemplatePage);
