<?
namespace CBitShedule;

class AddEvent {

	const DO_LOG = false;

	public function OnAfterIBlockElementAdd(&$arFields) {

		if (self::DO_LOG) {
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/dev/lo.log', print_r($arFields, true), FILE_APPEND);
		}

		if ($arFields['ID'] && ($arFields['PROPERTY_VALUES']['SH_F_TIME'])) {
			if (self::DO_LOG) {
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/dev/lo.log', print_r('Send to soap', true), FILE_APPEND);
			}
			self::SendAppointment($arFields['ID']);
		}

	}

	public static function SendAppointment($ElementID) {

		if (self::DO_LOG) {
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/dev/soap.log', print_r('BitrixElementId= ' . $ElementID, true) . PHP_EOL, FILE_APPEND);
		}

		if (!($ElementID = (int)$ElementID)) {
			return;
		}

		$REQUEST_IBLOCK_ID = \COption::GetOptionInt('firstbit.sheduler', 'REQUEST_IBLOCK_ID');

		$obElement = new \CIBlockElement();

		//@f:off
		if(
			\COption::GetOptionString('firstbit.shedule1c','USE_GENERATION')=='Y'
		)
		if($arRequest=
						$obElement->GetList(
													array(),
													array(
															'IBLOCK_ID'	=>	$REQUEST_IBLOCK_ID,
															'ID'		=>	$ElementID
														),
													false,
													false,
													array(
															'ID',
															'PROPERTY_SH_F_NAME',
															'PROPERTY_SH_F_PHONE',
															'PROPERTY_SH_F_EMAIL',
															'PROPERTY_SH_F_COMMENT',
															'PROPERTY_SH_F_DOCTOR.XML_ID',
															'PROPERTY_SH_F_DOCTOR.PROPERTY_IB_CLINIC',
															'PROPERTY_SH_F_DATE',
															'PROPERTY_SH_F_TIME',
															'PROPERTY_SH_F_TIME.PROPERTY_TM_TIME',
															'PROPERTY_SH_F_TIME.NAME'
														)
												)
												->Fetch()
			)
		if($arClinic=
      $obElement->GetByID(
             $arRequest['PROPERTY_SH_F_DOCTOR_PROPERTY_IB_CLINIC_VALUE']
            )
            ->Fetch()   )
  {
$arClinicXMLID=$arClinic['XMLID'];
				
			//@f:off
			list($connection,$result,$strConnectionResult)=ConnectionManager::Create(
																					\COption::GetOptionString("firstbit.shedule1c",'WSDL_URL'),
																					\COption::GetOptionString("firstbit.shedule1c",'WSDL_LOGIN'),
																					\COption::GetOptionString("firstbit.shedule1c",'WSDL_PASS'),
																					0
																					);
			
			if (!is_object($connection)) {
				return;
			}

					
			$arReserveParams=array	(
										'Specialization'	=>	'',
										'Date'				=>	($str_tmp_date=date('Y-m-d',$arRequest['PROPERTY_SH_F_TIME_PROPERTY_TM_TIME_VALUE'])).'T00:00:00',
										'TimeBegin'			=>	$str_tmp_date.'T'.date('H:i:s',$arRequest['PROPERTY_SH_F_TIME_PROPERTY_TM_TIME_VALUE']),
										'EmployeeID'		=>	$arRequest['PROPERTY_SH_F_DOCTOR_XML_ID'],
										'Clinic'			=>	$arClinicXMLID
									);
			

			$ResultReserve = $connection -> GetReserveUID($arReserveParams);
			
			if(self::DO_LOG) {
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/dev/soap.log', print_r(array('Function'=>'GetReserve',$arReserveParams, $ResultReserve), true).PHP_EOL, FILE_APPEND);
			}
			
			if ($ResultReserve['result']) {

				$arSoapParams=array(
								'EmployeeID'			=>	$arRequest['PROPERTY_SH_F_DOCTOR_XML_ID'],
								'PatientSurname'		=>	'-',
								'PatientName'			=>	$arRequest['PROPERTY_SH_F_NAME_VALUE'],
								'PatientFatherName'		=>	'',
								'Date'					=>	$arReserveParams['Date'],
								'TimeBegin'				=>	$arReserveParams['TimeBegin'],
								'Comment'				=>	$arRequest['PROPERTY_SH_F_COMMENT_VALUE'],
								'Phone'					=>	$arRequest['PROPERTY_SH_F_PHONE_VALUE'],
								'Email'					=>	$arRequest['PROPERTY_SH_F_EMAIL_VALUE'],
								'Address'				=>	'',
								'Clinic'				=>	$arClinicXMLID,
								'GUID'					=>	$ResultReserve['UID']
							);
		

				$Result = $connection -> SendAppointment($arSoapParams);
				
				if(self::DO_LOG) {
					file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/dev/soap.log', print_r(array('Function'=>'BookAnAppointment',$arSoapParams, $Result), true).PHP_EOL, FILE_APPEND);
				}
				
				if ($Result['result']) {
					return true;
				} else {
					$obElement->Update($arRequest['ID'], array('PREVIEW_TEXT' => '1'.$Result['error']));
					$obElement->Update($arRequest['PROPERTY_SH_F_TIME_VALUE'], array('PREVIEW_TEXT' => $Result['error']));
				}
			} else {
				$obElement->Update($arRequest['ID'], array('PREVIEW_TEXT' => '2'.$ResultReserve['error']));
				$obElement->Update($arRequest['PROPERTY_SH_F_TIME_VALUE'], array('PREVIEW_TEXT' => $ResultReserve['error']));
			}
		}
		return false;
		//@f:on
	}

}


