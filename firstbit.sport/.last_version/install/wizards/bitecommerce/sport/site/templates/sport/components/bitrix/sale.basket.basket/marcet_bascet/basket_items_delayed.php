<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
?>
<span class="my-wishlist-page">
<span class="my-wishlist">
<div id="basket_items_delayed" class="table-responsive" style="display:<?if($_GET['DELAYED']=='Y'){?>block<?}else{?>none<?}?>">
	<table class="table" id="delayed_items">

			<thead>
				<tr>
					<th colspan="4"><?=GetMessage("TITLE_LIVE_ORDER")?></th>
				</tr>
			</thead>
		<tbody>
			<?
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y"):
			?>
				<tr id="<?=$arItem["ID"]?>">
					
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

						if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in columns in this template
							continue;

						if ($arHeader["id"] == "NAME"):
						?>
							<td class="col-md-2">
							
								
									<?
									if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
										$url = $arItem["DETAIL_PICTURE_SRC"];
									else:
										$url = $templateFolder."/images/no_photo.png";
									endif;
									?>
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
										
										<img src="<?=$url?>" alt="imga">
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								
								<?
								if (!empty($arItem["BRAND"])):
								?>
								<div class="bx_ordercart_brand">
									<img alt="" src="<?=$arItem["BRAND"]?>" />
								</div>
								<?
								endif;
								?>
							</td>
							
					<td class="col-md-6">
						<div class="product-name"><?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
										<?=$arItem["NAME"]?>
									<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?></div>


				
							<div class="price">
								<?if (doubleval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
									<?=$arItem["PRICE_FORMATED"]?>
									<span><?=$arItem["FULL_PRICE_FORMATED"]?></span>
								<?else:?>
									<?=$arItem["PRICE_FORMATED"];?>
								<?endif?>

								<?if (strlen($arItem["NOTES"]) > 0):?>
									<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
									<div class="type_price_value"><?=$arItem["NOTES"]?></div>
								<?endif;?>
							</div>

					</td>

				<?endif;
					endforeach;?>
					<td class="col-md-2">
						<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["add"])?>" class="btn-upper btn btn-primary"><?=GetMessage("SALE_ADD_TO_BASKET")?></a>
					</td>
					<td class="col-md-2 close-btn">
						<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class=""><i class="fa fa-times"></i></a>
					</td>
				</tr>
				<?
				endif;
			endforeach;
			?>
		</tbody>

	</table>
</div>
</span>
</span>
<?