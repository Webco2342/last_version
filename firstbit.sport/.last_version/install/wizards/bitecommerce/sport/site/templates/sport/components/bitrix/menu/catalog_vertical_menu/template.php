<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["ALL_ITEMS"]))
	return;

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();
?>

<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="<?=$templateFolder?>/css/liMenuVert.css">

<script src="<?=$templateFolder?>/js/jquery.liMenuVert.js"></script>
<script>
$(function(){
	$('.menu_vert').liMenuVert({
		delayShow:300,		//Задержка перед появлением выпадающего меню (ms)
		delayHide:300	    //Задержка перед исчезанием выпадающего меню (ms)
	});
});
</script>

<div class="side-menu animate-dropdown outer-bottom-xs" id="<?=$menuBlockId?>">
<div class="head"><i class="icon fa fa-align-justify fa-fw"></i> <? echo GetMessage('CATALOG_MEDNU_TITLE'); ?></div>
<nav class="yamm megamenu-horizontal" role="navigation">
	<ul class="menu_vert" id="ul_<?=$menuBlockId?>">
	<?

	foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):
	?>     <!-- first level-->
		<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
		<li>
			<a <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>" >
				<?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?>				
			</a>
		<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<?foreach($arColumns as $key=>$arRow):
				$len = sizeof($arRow);
				$count = count($arRow);
				?>
					<ul>    
		
					<?					
					foreach($arRow as $itemIdLevel_2=>$arLevel_3):										
					?>
						
						<li>
							<a <?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>">
								<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>
							</a>
						
						<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
						
							<ul>
																
							<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
							
								<li>
								
									<a <?if($arResult["ALL_ITEMS"][$itemIdLevel_3]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>" >
										<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?>
									</a>
									
								</li>
							
							<?endforeach;?>
							
							</ul>
							
						<?endif?>
						</li>
						
					<?endforeach;?>
					
				
					</ul>
				<?endforeach;?>
		<?endif?>
		</li>
	<?endforeach;?>
	</ul>
	</nav>
	<div style="clear: both;"></div>
</div>