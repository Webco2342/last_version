<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

			<a href="<?=$arParams['PATH_TO_BASKET']?>" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
			<div class="items-cart-inner">
				<div class="total-price-basket">
					<span class="lbl"><?=GetMessage('TSB1_CART')?> -</span>
					<span class="total-price">						
						<span class="value">&nbsp;
						<?if ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'):?>
							<?=$arResult['TOTAL_PRICE']?>
						<?endif?>						
					</span>
				</div>
				<div class="basket">
					<i class="glyphicon glyphicon-shopping-cart"></i>
				</div>
				<div class="basket-item-count"><span class="count">		
				<?if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')):?>
					<?=$arResult['NUM_PRODUCTS']?>
				<?endif?>
				</span></div>
			
		    </div>
		</a>



