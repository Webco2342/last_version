<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0)
{
?>
	<ul id="bascet" class="dropdown-menu">
	

		<?if ($arParams["POSITION_FIXED"] == "Y"):?>
		<hr>
			<div id="<?=$cartId?>status" class="btn btn-upper btn-primary btn-block m-t-20" onclick="<?=$cartId?>.toggleOpenCloseCart()"><?=GetMessage("TSB1_COLLAPSE")?></div>
		<?endif?>



		
			<?foreach ($arResult["CATEGORIES"] as $category => $items):
				if (empty($items))
					continue;
				?>
				<?foreach ($items as $key=>$v):?>
				<li>
				
				<div class="cart-item product-summary">
					<div class="row">
						<div class="col-xs-4">
							<div class="image">								
								<?if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]):?>
								<?if($v["DETAIL_PAGE_URL"]):?>
									<a href="<?=$v["DETAIL_PAGE_URL"]?>"><img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>"></a>
								<?else:?>
									<img src="<?=$v["PICTURE_SRC"]?>" alt="<?=$v["NAME"]?>" />
								<?endif?>
							<?endif?>
							</div>
						</div>
						<div class="col-xs-7">							
							<h3 class="name">
							<?if ($v["DETAIL_PAGE_URL"]):?>
								<a href="<?=$v["DETAIL_PAGE_URL"]?>"><?=$v["NAME"]?></a>
							<?else:?>
								<?=$v["NAME"]?>
							<?endif?>
							</h3>
							<div class="price">

							<div class="price"><?=$v["QUANTITY"]?> <?=$v["MEASURE_NAME"]?> </div> 
							<div class="price"><?=$v["SUM"]?></div>
							</div>
						</div>
						<div class="col-xs-1 action" onclick="<?=$cartId?>.removeItemFromCart(<?=$v['ID']?>)" title="<?=GetMessage("TSB1_DELETE")?>">
							<a><i class="fa fa-trash"></i></a>
							
						</div>
					</div>
				</div><!-- /.cart-item -->
				<div class="clearfix"></div>
			<hr>
				</li>
				<?endforeach?>
			<?endforeach?>
		<?if($arParams["PATH_TO_ORDER"] && $arResult["CATEGORIES"]["READY"]):?>
		<li>
			<div class="clearfix cart-total">
				<a href="<?=SITE_DIR?>personal/cart/" class="btn btn-upper btn-primary btn-block m-t-20"><?=GetMessage("TSB1_2ORDER")?></a>
			</div>
		</li>
		<?endif?>		
	</ul>

	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
		});
	</script>
<?
}
?>
