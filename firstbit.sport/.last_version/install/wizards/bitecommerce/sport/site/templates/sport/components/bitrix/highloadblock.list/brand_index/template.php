<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}

$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/js/highloadblock/css/highloadblock.css');

//$GLOBALS['APPLICATION']->SetTitle('Highloadblock List');

?>

		<div class="logo-slider-inner">	
			<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
	<!-- data -->
	<? foreach ($arResult['rows'] as $row): ?>

					<div class="item">
					<a href="<?=SITE_DIR?>brands/<?=$row['ID']?>-<?=$row['UF_XML_ID']?>.php" class="image">
						<?=$row['UF_FILE']?>
					</a>	
				</div><!--/.item-->

		<? endforeach; ?>



		    </div><!-- /.owl-carousel #logo-slider -->
		</div><!-- /.logo-slider-inner -->