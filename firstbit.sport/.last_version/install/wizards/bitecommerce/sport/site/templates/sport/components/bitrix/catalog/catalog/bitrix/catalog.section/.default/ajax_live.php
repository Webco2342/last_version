<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if (CModule::IncludeModule("sale"))
{
	$rubric = new CSaleBasket;
  $arFields = array(
    "PRODUCT_ID" => $_GET['PRODUCT_ID'],
    "PRICE" => $_GET['PRICE'],
    "CURRENCY" => $_GET['CURRENCY'],
    "LID" => $_GET['LID'],
    "NAME" => $_GET['NAME'],
	"PREVIEW_PICTURE" => $_GET['IMG'],
	"DISCOUNT_PRICE" => $_GET['DISCOUNT'],
	"DISCOUNT_VALUE" => $_GET['DISCOUNT_PROC'],
	"DETAIL_PAGE_URL" => $_GET['DETAIL_PAGE_URL'],
	"DELAY" => "Y",
  );


  $ID = $rubric->Add($arFields);
if($ID == false)
    echo $rubric->LAST_ERROR;
}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>