<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

CUtil::InitJSCore(array('popup'));

$arResult = array();
if (CModule::IncludeModule("iblock")):
if(!isset($arParams["CACHE_TIME"]))
$arParams["CACHE_TIME"] = 36000000;
$arSelect = Array(
"ID", 
"NAME", 
"CODE", 
"PREVIEW_PICTURE",
"IBLOCK_ID",
"PROPERTY_URL",
"PROPERTY_SECTION",
);
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ACTIVE"=>"Y");

if($_REQUEST["SECTION_CODE"]){

 $Filter = Array('CODE'=>$_REQUEST["SECTION_CODE"], );
  $db_list = CIBlockSection::GetList(Array("SORT"=>"ASC"), $Filter, true);
  while($ar_result = $db_list->GetNext())
  {	
$arFilter['PROPERTY_SECTION'] = $ar_result['ID'];	
  }
}else{
$arFilter["CODE"] = $arParams['ELEMENT_CODE'];
if($arParams['CATALOG']=="Y"){
$arFilter["SECTION_CODE"] = "CATALOG";
$arFilter['PROPERTY_SECTION'] = false;
}
}

$res = CIBlockElement::GetList(Array("RAND"=>"ASC"), $arFilter, false, Array("nPageSize"=>'1'), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arButtons = CIBlock::GetPanelButtons(
         $arFields["IBLOCK_ID"],
         $arFields["ID"],
         0,
         array("SECTION_BUTTONS"=>false, "SESSID"=>false)
      );
$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
$arResult['ITEMS'][] = $arFields;
};
    $this->IncludeComponentTemplate();
    endif;
?>