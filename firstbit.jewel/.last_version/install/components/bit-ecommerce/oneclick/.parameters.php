<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
global $USER_FIELD_MANAGER;

if(!\Bitrix\Main\Loader::includeModule("iblock") || !\Bitrix\Main\Loader::includeModule("sale"))
	return;

$boolCatalog = \Bitrix\Main\Loader::includeModule("catalog");

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arProperty = array();
$arProperty_N = array();
$arProperty_X = array();
if (0 < intval($arCurrentValues["IBLOCK_ID"]))
{
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"], "ACTIVE"=>"Y"));
	while ($arr=$rsProp->Fetch())
	{
		if($arr["PROPERTY_TYPE"] != "F")
			$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];

		if($arr["PROPERTY_TYPE"] == "N")
			$arProperty_N[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];

		if ($arr["PROPERTY_TYPE"] != "F")
		{
			if($arr["MULTIPLE"] == "Y")
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
			elseif($arr["PROPERTY_TYPE"] == "L")
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
			elseif($arr["PROPERTY_TYPE"] == "E" && $arr["LINK_IBLOCK_ID"] > 0)
				$arProperty_X[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
		}
	}
}
$arProperty_LNS = $arProperty;

$arIBlock_LINK = array();
$rsIblock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["LINK_IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIblock->Fetch())
	$arIBlock_LINK[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];

$arProperty_LINK = array();
if (0 < intval($arCurrentValues["LINK_IBLOCK_ID"]))
{
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$arCurrentValues["LINK_IBLOCK_ID"], 'PROPERTY_TYPE' => 'E', "ACTIVE"=>"Y"));
	while ($arr=$rsProp->Fetch())
	{
		$arProperty_LINK[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}

$arUserFields_S = array("-"=>" ");
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_".$arCurrentValues["IBLOCK_ID"]."_SECTION");
foreach($arUserFields as $FIELD_NAME=>$arUserField)
	if($arUserField["USER_TYPE"]["BASE_TYPE"]=="string")
		$arUserFields_S[$FIELD_NAME] = $arUserField["LIST_COLUMN_LABEL"]? $arUserField["LIST_COLUMN_LABEL"]: $FIELD_NAME;

$arOffers = CIBlockPriceTools::GetOffersIBlock($arCurrentValues["IBLOCK_ID"]);
$OFFERS_IBLOCK_ID = is_array($arOffers)? $arOffers["OFFERS_IBLOCK_ID"]: 0;
$arProperty_Offers = array();
$arProperty_OffersWithoutFile = array();
if($OFFERS_IBLOCK_ID)
{
	$rsProp = CIBlockProperty::GetList(array("sort"=>"asc", "name"=>"asc"), array("IBLOCK_ID"=>$OFFERS_IBLOCK_ID, "ACTIVE"=>"Y"));
	while($arr=$rsProp->Fetch())
	{
		$arr['ID'] = intval($arr['ID']);
		if ($arOffers['OFFERS_PROPERTY_ID'] == $arr['ID'])
			continue;
		$strPropName = '['.$arr['ID'].']'.('' != $arr['CODE'] ? '['.$arr['CODE'].']' : '').' '.$arr['NAME'];
		if ('' == $arr['CODE'])
			$arr['CODE'] = $arr['ID'];
		$arProperty_Offers[$arr["CODE"]] = $strPropName;
		if ('F' != $arr['PROPERTY_TYPE'])
			$arProperty_OffersWithoutFile[$arr["CODE"]] = $strPropName;
	}
}

$arSort = CIBlockParameters::GetElementSortFields(
	array('SHOWS', 'SORT', 'TIMESTAMP_X', 'NAME', 'ID', 'ACTIVE_FROM', 'ACTIVE_TO'),
	array('KEY_LOWERCASE' => 'Y')
);

$arPrice = array();
if ($boolCatalog)
{
	$rsPrice=CCatalogGroup::GetList($v1="sort", $v2="asc");
	while($arr=$rsPrice->Fetch()) $arPrice[$arr["NAME"]] = "[".$arr["NAME"]."] ".$arr["NAME_LANG"];
}
else
{
	$arPrice = $arProperty_N;
}

$arAscDesc = array(
	"asc" => GetMessage("IBLOCK_SORT_ASC"),
	"desc" => GetMessage("IBLOCK_SORT_DESC"),
);


$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));
$arFilter = Array( "ACTIVE" => "Y");
if($site !== false)
	$arFilter["LID"] = $site;

$arEvent = Array();
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

$PSpersonType = array();
$dbPersonType = CSalePersonType::GetList(Array("ID" => "ASC", "NAME" => "ASC"), Array(/*"LID" => SITE_ID, */"ACTIVE" => "Y"));
while($arPersonType = $dbPersonType->GetNext())
{
	$PSpersonType[$arPersonType["ID"]] = "[".$arPersonType["ID"]."] ".$arPersonType["NAME"];
}


$db_ptype = CSalePaySystem::GetList($arOrder = Array("ID"=>"ASC", "PSA_NAME"=>"ASC"), Array(/*"LID"=>SITE_ID, */"ACTIVE"=>"Y", "PERSON_TYPE_ID"=>$arCurrentValues['PERSON_TYPE_ID']));
$arPaySystem = array();
while ($ptype = $db_ptype->Fetch())
{
   $arPaySystem[$ptype['ID']] = "[".$ptype["ID"]."] ".$ptype['NAME'];
}

$db_dtype = CSaleDelivery::GetList($arOrder = Array("ID"=>"ASC"),array("ACTIVE" => "Y", "PERSON_TYPE_ID"=>$arCurrentValues['PERSON_TYPE_ID']));
$arDelivery = array();
while ($ar_dtype = $db_dtype->Fetch())
{
   $arDelivery[$ar_dtype['ID']] = "[".$ar_dtype["ID"]."] ".$ar_dtype['NAME'];
}

$db_props = CSaleOrderProps::GetList(array("ID" => "ASC"),array("PERSON_TYPE_ID" => $arCurrentValues['PERSON_TYPE_ID']));
$arPropsOrder = array();
while ($props = $db_props->Fetch()){
	$arPropsOrder[$props['ID']] = "[".$props["ID"]."] ".$props['NAME'];
}
unset($arPropsOrder[2]);
unset($arPropsOrder[3]);
unset($arPropsOrder[13]);
unset($arPropsOrder[14]);
$arComponentParameters = array(
	"GROUPS" => array(
		"MODE" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_MODE"),
		),
		"SELECT_FIELD" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_SELECT_FIELD"),
		),
		"USER_SETTINGS" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_USER_SETTINGS"),
		),
		"PRICES_SETTINGS" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_PRICES_SETTINGS"),
		),
		"SEND_SETTINGS" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_SEND_SETTINGS"),
		),
        "LIST_SETTINGS" => array(
			"NAME" => GetMessage("H2O_BUYONECLICK_LIST_SETTINGS"),
		),
        
		
		
		
	),
	"PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"ELEMENT_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("H2O_BUYONECLICK_ELEMENT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["ELEMENT_ID"]}',
		),
		"PRICE" => array(
			"PARENT" => "BASE",
			"NAME" => '����',
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"DISCOUNT" => array(
			"PARENT" => "BASE",
			"NAME" => '����',
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"DISCOUNT_PROC" => array(
			"PARENT" => "BASE",
			"NAME" => '����',
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"DETAIL_PAGE_URL" => array(
			"PARENT" => "BASE",
			"NAME" => '����',
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"OK_TEXT" => Array(
			"NAME" => GetMessage("H2O_BUYONECLICK_OK_MESSAGE"), 
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("H2O_BUYONECLICK_OK_TEXT"), 
			"PARENT" => "BASE",
		),
		
		
		
	),
);






		$arComponentParameters["PARAMETERS"]["FIX_USER_ID"] = array(
			"PARENT" => "USER_SETTINGS",
			"NAME" => GetMessage("H2O_COUNTDOWN_FIX_USER_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		);

	

echo $OFFERS_IBLOCK_ID;
?>