<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
if (CModule::IncludeModule("sale")){
if (CModule::IncludeModule("catalog"))
{
?>


<div class="tab-container body-content" style="display: none;">
    <div id="buy_one_click_ajaxwrap" class="buy_one_click_ajaxwrap sign-in-page inner-bottom-sm">

<p> </p>
	<div class="sign-in sidebar-wrap-dept">
<div class="appointment-form no-pad dept-form">



	

<div id="resultf">
<?=CAjax::GetForm('name="ajaxform" action="'.$templateFolder.'/ajax.php" method="POST" class="appt-form" enctype="multipart/form-data"', 'resultf', '1', 'Y', 'Y');?>   

<input type="hidden" name="bxajaxid" id="bxajaxid_<?=$bxajaxid?>_<?=$arResult["RND"]?>" value="<?=$arResult['AJAX_ID']?>" />
<input type="hidden" name="AJAX_CALL" value="Y" />	
<input type="hidden" name="ONE_CLICK" value="Y" />
<input type="hidden" class="SUCCESS" value="Y" />

<input type="hidden" name="ELEMENT_ID" value="<?=$arParams['ELEMENT_ID']?>" />	
<input type="hidden" name="PRICE" value="<?=$arParams['PRICE']?>" />	
<input type="hidden" name="LID" value="<?=$arParams['LID']?>" />	
<input type="hidden" name="NAME" value="<?=$arParams['NAME']?>" />
<input type="hidden" name="PERSON_TYPE_ID" value="<?=$arParams['PERSON_TYPE_ID']?>" />
<input type="hidden" name="RES" value="<?=GetMessage('TEXT')?>" />



<div class="form-group">
		<input name="user_name" class="form-control unicase-form-control text-inputt" type="text" required placeholder="<?=GetMessage('NAME'); ?>" value="">
</div>
<div class="form-group">
		<input name="user_tel" class="form-control unicase-form-control text-inputt" type="text" required placeholder="<?=GetMessage('PERSONAL_PHONE'); ?>">
</div>	
	 <div class="btn_form pull-right"><input class="btn btn-primary" type="submit" name="submit" value="<?=GetMessage('BUY_ONE_CLICK_BUY'); ?>"/></div>	
 

</form>	
</div>
</div>
</div>
</div>

</div>



<button class="bx_big bx_bt_button css_popup"><?=GetMessage('BUY_ONE_CLICK'); ?></button>

<script type="text/javascript">
	BX.ready(function(){
	   var oPopup = new BX.PopupWindow('call_buyoneclick',
	    null, 
		{
		  content: BX( 'buy_one_click_ajaxwrap'), 	
	      autoHide : false,
	      titleBar: {content: BX.create("span", {html: '<b><?=GetMessage('BUY_ONE_CLICK'); ?></b>', 'props': {'className': 'access-title-bar'}})}, 
	      offsetTop : 1,
	      offsetLeft : 0,
	      lightShadow : true,
	      closeIcon : true,
	      closeByEsc : true,
	      draggable: {restrict: false},
	      overlay: {
	         backgroundColor: 'grey', opacity: '80'
	      },
	   });
	   //oPopup.setContent(BX('delivery_energy').innerHTML);
	   BX.bindDelegate(
	      document.body, 'click', {className: 'css_popup' },
	         BX.proxy(function(e){
	            if(!e)
	               e = window.event;
	            oPopup.show();
	            return BX.PreventDefault(e);
	         }, oPopup)
	   );
	   
	   
	   
	});
</script>


<script>/*
$(document).on('submit','.buy_one_click_form',function(e){
	e.preventDefault();
	var $this = $(this);
	$(this).bitrixAjax(
		"<?=$arResult['AJAX_ID']?>",
		function (data) { 
			console.log(data);
			$('.buy_one_click_ajaxwrap').html(data.find(".buy_one_click_ajaxwrap").html());
		}
	);
});*/
</script>
<?}}?>