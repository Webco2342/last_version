<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.small", 
	"top_small", 
	array(
		"COMPONENT_TEMPLATE" => "top_small",
		"PATH_TO_BASKET" => SITE_DIR."personal/basket.php",
		"PATH_TO_ORDER" => SITE_DIR."personal/order.php",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N"
	),
	false
);?>
