<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
?>
<?$arPlacemarks=array();?>
<div class="catalog-detail-properties_sam contact-page">
	<div class="catalog-detail-line"></div>
	<?if(is_array($arResult["STORES"]) && !empty($arResult["STORES"])):?>
	<?foreach($arResult["STORES"] as $pid=>$arProperty):?>
<div class="col-md-4 contact-info">
			<div class="contact-title">
				<h4><?=$arProperty["ADDRESS"]?></h4>
			</div>
			
			<div class="clearfix address">			
			<span class="contact-i"><i class="fa fa-map-marker"></i></span> <span class="contact-span"><?=$arProperty["TITLE"]?>
			</span>
			</div>
			
			<div class="clearfix address">
			<? if(isset($arProperty["SCHEDULE"])):?>
			<span class="contact-i"><i class="fa fa-calendar"></i></span> 
			<span class="contact-span"><?=$arProperty["SCHEDULE"]?></span>
			<?endif;?>
			</div>			

			<div class="clearfix phone-no"> 
 		<? if(isset($arProperty["PHONE"])):?>
		<span class="contact-i"><i class="fa fa-mobile"></i></span> 
		<span class="contact-span"><?=$arProperty["PHONE"]?></span>
		<?endif;?> 
			</div>
			
			<a href="<?=$arProperty["URL"]?>" class="btn btn-primary"><?=GetMessage('S_POD')?></a>
			
		</div>
				<?
		if($arProperty["GPS_S"]!=0 && $arProperty["GPS_N"]!=0)
		{
			$gpsN=substr(doubleval($arProperty["GPS_N"]),0,15);
			$gpsS=substr(doubleval($arProperty["GPS_S"]),0,15);
			$arPlacemarks[]=array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arProperty["TITLE"]);
		}
		?>
	<?endforeach;?>
	<?endif;?>

<div class="col-md-12 contact-map outer-bottom-vs">
<?
if ($arResult['VIEW_MAP'])
{
	if($arResult["MAP"]==0)
	{
		$APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => serialize(array("yandex_lat"=>$gpsN,"yandex_lon"=>$gpsS,"yandex_scale"=>10,"PLACEMARKS" => $arPlacemarks)),
				"MAP_WIDTH" => "100%",
				"MAP_HEIGHT" => "500",
				"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
				),
				"OPTIONS" => array(
					0 => "ENABLE_DBLCLICK_ZOOM",
					1 => "ENABLE_DRAGGING",
				),
				"MAP_ID" => ""
			),
			false
		);
	}
	else
	{
		$APPLICATION->IncludeComponent("bitrix:map.google.view", ".default", array(
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>10,"PLACEMARKS" => $arPlacemarks)),
				"MAP_WIDTH" => "100%",
				"MAP_HEIGHT" => "500",
				"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
				),
				"OPTIONS" => array(
					0 => "ENABLE_DBLCLICK_ZOOM",
					1 => "ENABLE_DRAGGING",
				),
				"MAP_ID" => ""
			),
			false
		);
	}
}
?>
</div>
</div>