<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="catalog-detail contact-page" itemscope itemtype = "http://schema.org/Product">






	<div class="col-md-9 contact-info">
			<div class="contact-title">
				<h4><?=GetMessage("S_NAME")." ".$arResult["TITLE"];?></h4>
			</div>
		
				<?
			if(intval($arResult["IMAGE_ID"]) > 0)
			{
				?>
				
					<div style="padding-bottom:15px;" class="catalog-detail-image clearfix" id="catalog-detail-main-image">
						<?echo CFile::ShowImage($arResult["IMAGE_ID"], 300, 250, "border=0", "", true);?>

					</div>
				
				<?
			}
			?>
	
			<div class="clearfix address">
				<?if($arResult["DESCRIPTION"]):?>
				<span class="contact-i"><i class="fa fa-font"></i></span> 
				<span class="contact-span" itemprop = "description"><?=$arResult["DESCRIPTION"];?></span>				
				<?endif;?>
			</div>
			<div class="clearfix address">
			<?if ($arResult["SCHEDULE"] != ''):?>
			<span class="contact-i"><i class="fa fa-calendar"></i></span> 
			<span class="contact-span"><?=GetMessage("S_SCHEDULE")." ".$arResult["SCHEDULE"];?></span>
			<?endif;?>
			</div>
			<div class="clearfix address">
			<?if($arResult["ADDRESS"]):?>
			 <span class="contact-i"><i class="fa fa-map-marker"></i></span> <span class="contact-span"><?=GetMessage("S_ADDRESS")." ".$arResult["ADDRESS"];?>
			 </span>
			 <?endif;?>
			</div>
			<div class="clearfix phone-no"> 
 		<?if($arResult["PHONE"] != ''):?>
		<span class="contact-i"><i class="fa fa-mobile"></i></span> 
		<span class="contact-span"><?=GetMessage("S_PHONE")." ".$arResult["PHONE"];?></span>
		<?endif;?> 
			</div>
		</div>

			<?
			if(isset($arResult["LIST_URL"]))
			{
				?>
				<div class="catalog-item-links">
					<a href="<?=$arResult["LIST_URL"]?>"  class="btn btn-primary"><?=GetMessage("BACK_STORE_LIST")?>  </a>
				</div>
				<?
			}
			?>		
<div style="clear:both"><br></div>
	<div id="map" class="catalog-detail-recommend">
		<?
		if(($arResult["GPS_N"]) != 0 && ($arResult["GPS_S"]) != 0)
		{
			$gpsN = substr($arResult["GPS_N"],0,15);
			$gpsS = substr($arResult["GPS_S"],0,15);
			$gpsText = $arResult["ADDRESS"];
			$gpsTextLen = strlen($arResult["ADDRESS"]);
			if($arResult["MAP"] == 0)
			{
				$APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
						"INIT_MAP_TYPE" => "MAP",
						"MAP_DATA" => serialize(array("yandex_lat"=>$gpsN,"yandex_lon"=>$gpsS,"yandex_scale"=>13,"PLACEMARKS" => array( 0=>array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arResult["ADDRESS"])))),
						"MAP_WIDTH" => "100%",
						"MAP_HEIGHT" => "500",
						"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
						),
						"OPTIONS" => array(
					0 => "ENABLE_DBLCLICK_ZOOM",
					1 => "ENABLE_DRAGGING"
						),
						"MAP_ID" => ""
					),
					false
				);
			}
			else
			{
				$APPLICATION->IncludeComponent("bitrix:map.google.view", ".default", array(
						"INIT_MAP_TYPE" => "MAP",
						"MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>13,"PLACEMARKS" => array( 0=>array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arResult["ADDRESS"])))),
						"MAP_WIDTH" => "100%",
						"MAP_HEIGHT" => "500",
						"CONTROLS" => array(
					0 => "ZOOM",
					1 => "MINIMAP",
					2 => "TYPECONTROL",
					3 => "SCALELINE",
						),
						"OPTIONS" => array(
					0 => "ENABLE_DBLCLICK_ZOOM",
					1 => "ENABLE_DRAGGING"
						),
						"MAP_ID" => ""
					),
					false
				);
			}
		}
		?>
	</div>
</div>