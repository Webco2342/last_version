<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult["ALL_ITEMS"]))
	return;

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();
?>
<div class="side-menu animate-dropdown outer-bottom-xs" id="<?=$menuBlockId?>">
<div class="head"><i class="icon fa fa-align-justify fa-fw"></i> <? echo GetMessage('CATALOG_MEDNU_TITLE'); ?></div>
<nav class="yamm megamenu-horizontal" role="navigation">
	<ul class="nav" id="ul_<?=$menuBlockId?>">
	<?

	foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):
	?>     <!-- first level-->
		<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
		<li class="menu-item <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>current<?endif?><?if (is_array($arColumns) && count($arColumns) > 0):?> dropdown<?endif?>">
			<a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>" <?if (is_array($arColumns) && count($arColumns) > 0):?>class="dropdown-toggle" data-toggle="dropdown"<?endif?>>
				<?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?>
				<span class="bx_shadow_fix"></span>
			</a>
		<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<?foreach($arColumns as $key=>$arRow):
				$len = sizeof($arRow);
				$count = count($arRow);
				?>
					<ul style="<?if($len <= '4'){?> min-width: 83%; <?}elseif(($len > '4') && ($len < '8')){?> min-width: 166%; <?}elseif(($len > '8') && ($len < '12')){?> min-width: 249%; <?}else{?> min-width: 330%; <?}?>" class="dropdown-menu mega-menu">
    <li class="yamm-content">
        <div class="row"> 
		
					<?					
					if($count<=4){
 					$col = 1;
					}else{
					$col = 4;
					}
					$if = ceil($count/$col);	
					$i = 0;
					foreach($arRow as $itemIdLevel_2=>$arLevel_3):
					--$len;										
					if($i==0){					 
					?> <!-- second level-->
            <div class="<?if($count <= '4'){?> col-sm-12 col-md-12 <?}elseif(($count > '4') && ($count < '8')){?> col-sm-12 col-md-6 <?}elseif(($count > '8') && ($count < '12')){?> col-sm-12 col-md-3 <?}else{?> col-sm-12 col-md-3 <?}?>"> 
                <ul class="links list-unstyled"> 
					<?}					
					?>
						<li>
							<a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>" <?if ($existPictureDescColomn):?>ontouchstart="document.location.href = '<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>';" onmouseover="BX.CatalogVertMenu.changeSectionPicture(this);"<?endif?> data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>">
								<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>
							</a>
						<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
							<ul>
								<div class="row">									
							<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
							<div class="col-sm-12 col-md-3">
								<li class="links list-unstyled">
									<a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>" <?if ($existPictureDescColomn):?>ontouchstart="document.location.href = '<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>';return false;" onmouseover="BX.CatalogVertMenu.changeSectionPicture(this);return false;"<?endif?> data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"]?>">
										<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?>
									</a>
								</li>
							</div>
							<?endforeach;?>
							</div>
							</ul>
						<?endif?>
						</li>
						<?if($if==$i%4){?>
						</ul>
						</div>	
						<div class="<?if($count <= '4'){?> col-sm-12 col-md-12 <?}elseif(($count > '4') && ($count < '8')){?> col-sm-12 col-md-6 <?}elseif(($count > '8') && ($count < '12')){?> col-sm-12 col-md-3 <?}else{?> col-sm-12 col-md-3 <?}?>">
						<ul class="links list-unstyled"> 
						<?}?>
						<?if(!$len) {?>							
						</ul>
						</div>	
						<?}
						++$i;
						?>
					<?endforeach;?>
					</div>
					</li>
					</ul>
				<?endforeach;?>
		<?endif?>
		</li>
	<?endforeach;?>
	</ul>
	</nav>
	<div style="clear: both;"></div>
</div>