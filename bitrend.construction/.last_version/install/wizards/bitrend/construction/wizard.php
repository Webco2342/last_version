<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
	function InitStep()
	{
		parent::InitStep();

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "construction";
        
        $this->SetPrevStep("license_agreement");
        $this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));
	}
}

class SelectTemplateStep extends CSelectTemplateWizardStep
{
}

class SelectThemeStep extends CSelectThemeWizardStep
{
}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "construction";
		parent::InitStep();

		$templateID = $wizard->GetVar("templateID");
		$themeID = $wizard->GetVar($templateID."_themeID");
        $templatePath = $_SERVER["DOCUMENT_ROOT"].BX_PERSONAL_ROOT."/templates/".$templateID."/";
        $wizardTemplatePath = "/bitrix/wizards/bitrend/construction/site/templates/".$templateID;

        $siteLogo = "/bitrix/wizards/bitrend/construction/site/templates/construction/img/logo.png";
		$siteHeader = "/bitrix/wizards/bitrend/construction/site/templates/construction/img/home_bg.jpg";
		$wizard->SetDefaultVars(
			Array(
				"siteLogo" => $siteLogo,
				"siteHeader" => $siteHeader,
				"siteCopyrightText" => $this->GetFileContent(WIZARD_TEMPLATE_ABSOLUTE_PATH."include_areas/copyright.php", GetMessage("WIZ_COPYRIGHT_DEF")),
			)
		);
	}

	function ShowStep()
	{
	    $arSelectBlogSection = array(
            GetMessage("WIZ_HEADER_1"),
            GetMessage("WIZ_HEADER_2"),
            GetMessage("WIZ_HEADER_3"),
        );
        
        $arSelectTypeHeader = array(
            GetMessage("WIZ_TYPE_HEADER_1"),
            GetMessage("WIZ_TYPE_HEADER_2"),
            GetMessage("WIZ_TYPE_HEADER_3"),
        );
        
		$wizard =& $this->GetWizard();
        
        $siteLogo = $wizard->GetVar("siteLogo", true);
        //echo $siteLogo;
        $siteLogoShow = CFile::ShowImage($siteLogo, 123, 30);

		$this->content .= '<table width="100%" cellspacing="0" cellpadding="0">';
        
        $this->content .= '<tr><td><h2>'.GetMessage("SETTINGS_MAIN").'</h2></tr></td>';
        
        $this->content .= '<tr><td>';
        $this->content .= '<strong>'.GetMessage("WIZ_LOGO").'</strong><br />';
        $this->content .= $siteLogoShow."<br/>";
        $this->content .= $this->ShowFileField("siteLogo", Array("show_file_info" => "N", "id" => "site-logo"));
        $this->content .= '<p style="margin-top:0!important;">'.GetMessage("WIZ_LOGO_DESC").'</p>';
        $this->content .= '</tr></td>';
		$this->content .= '<tr><td><br /></td></tr>';
        
        //Header site
		 $siteHeader = $wizard->GetVar("siteHeader", true);
        $siteHeaderShow = CFile::ShowImage($siteHeader, 600, 313);
        $this->content .= '<tr><td>';
        $this->content .= '<strong>'.GetMessage("WIZ_HEADER").'</strong><br />';
        $this->content .= $siteHeaderShow."<br/>";
        $this->content .= $this->ShowFileField("siteHeader", Array("show_file_info" => "N", "id" => "site-Headr"));
        $this->content .= '<p style="margin-top:0!important;">'.GetMessage("WIZ_HEADER_DESC").'</p>';
        $this->content .= '</tr></td>';
		$this->content .= '<tr><td><br /></td></tr>';
        
     
        //Copyright
		$this->content .= '<tr><td>';
		$this->content .= '<label for="copytext"><strong>'.GetMessage("WIZ_COPYRIGHT").'</strong></label><br />';
		$this->content .= $this->ShowInputField("textarea", "siteCopyrightText", Array("id" => "copytext", "style" => "width:100%", "rows"=>"3"));
		$this->content .= '</tr></td>';
		$this->content .= '<tr><td><br /></td></tr>';
        
       
        

           
       
        
     		$this->content .= '</table>';
                
		$this->content .= $this->ShowHiddenField("installDemoData","Y");
		
		$formName = $wizard->GetFormName();
		$installCaption = $this->GetNextCaption();
		$nextCaption = GetMessage("NEXT_BUTTON");
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();
        $res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 30, "max_width" => 123));
		$res = $this->SaveFile("siteHeader", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 667, "max_width" => 1278));
	

	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function CorrectServices(&$arServices)
	{
		$wizard =& $this->GetWizard();
		if($wizard->GetVar("installDemoData") != "Y")
		{
		}
	}
}

class FinishStep extends CFinishWizardStep
{
}
?>