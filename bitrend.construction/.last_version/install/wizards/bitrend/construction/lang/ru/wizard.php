<?
$MESS["WIZ_COPYRIGHT"]              = "������� � \"�������\" �����";
$MESS["WIZ_COPYRIGHT_DEF"]          = "&copy; Copyright 2015";
$MESS["WIZ_TWITTER"]                = "����� Twitter";
$MESS["WIZ_TWITTER_DEF"]            = "";
$MESS["WIZ_TWITTER_DESC"]           = "���� ���� �������� ������, �� ������� ����� ����� ���������.";
$MESS["WIZ_HEADER"]                 = "����������� � \"�����\" �����";
$MESS["WIZ_HEADER_1"]               = "�������";
$MESS["WIZ_HEADER_2"]               = "�������";
$MESS["WIZ_HEADER_3"]               = "����";
$MESS["WIZ_TYPE_HEADER"]            = "��� \"�����\" �����";
$MESS["WIZ_TYPE_HEADER_1"]          = "�������";
$MESS["WIZ_TYPE_HEADER_2"]          = "�����������";
$MESS["WIZ_TYPE_HEADER_3"]          = "�����������";
$MESS["WIZ_PHONE"]                  = "�������";
$MESS["WIZ_EMAIL"]                  = "Email";
$MESS["WIZ_VKPAGE"]                 = "�������� ���������";
$MESS["WIZ_FBPAGE"]                 = "�������� Facebook";
$MESS["WIZ_LOGO"]                   = "������� �����";
$MESS["WIZ_LOGO_DESC"]              = "������ � ������ �� ������ ��������� 123 �� 30 �������.";
$MESS["WIZ_HEADER_DESC"]              = "������ � ������ �� ������ ��������� 1278 �� 667 �������.";
$MESS["SETTINGS_TWITTER"]           = "��������� ������� ��������";
$MESS["SETTINGS_CONTACTS"]          = "����� �������� �����";
$MESS["SETTINGS_FOOTER"]            = "��������� \"������\" �����";
$MESS["SETTINGS_MAIN"]              = "�������� ���������";

$MESS["V1RT_PERSONAL_TWITTER_CONSUMER_KEY"]     = "���� (Consumer key)";
$MESS["V1RT_PERSONAL_TWITTER_CONSUMER_SECRET"]  = "��������� ������ (Consumer secret)";
$MESS["V1RT_PERSONAL_TWITTER_USER_TOKEN"]       = "����� (Access token)";
$MESS["V1RT_PERSONAL_TWITTER_USER_SECRET"]      = "��������� ������ (Access token secret)";
?>