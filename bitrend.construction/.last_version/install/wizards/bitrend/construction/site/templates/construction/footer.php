<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
		<!-- Clear :) -->
		<div class="clear"></div>
	</div>
	<!-- End Content -->
	
	<footer class="footer">
		<h6><?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath(SITE_DIR."include_areas/copyright.php"),
            Array(),
            Array("MODE"=>"html")
        );?></h6>
	</footer>
		<?
		$APPLICATION->addHeadString('<link rel="stylesheet" href="'.SITE_TEMPLATE_PATH.'/css/webfont.css" >');
		$APPLICATION->AddHeadScript("//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.jcarousel.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.mixitup.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/custom.js");
		?>

	</body>
</html>