<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 			
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
			<div class="menubar" data-scroll="true">
		<a href="#" class="logo" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/logo.#EXT#)"></a>
									<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
	"ROOT_MENU_TYPE" => "top",	// ��� ���� ��� ������� ������
		"MENU_CACHE_TYPE" => "N",	// ��� �����������
		"MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
		"MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
		"MAX_LEVEL" => "2",	// ������� ����������� ����
		"CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
		"USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
		"DELAY" => "N",	// ����������� ���������� ������� ����
		"ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
	),
	false
);?>
		</div>				
			<a class="scroll" id="home"></a>
	<div class="home hero" style="background-image: url(<?=SITE_TEMPLATE_PATH?>/img/home_bg.#EXTR#); background-size: cover;" >
		<div class="overlay"></div>
		<div class="herowrapper">
			<h1><?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath(SITE_DIR."include_areas/slogan.php"),
            Array(),
            Array("MODE"=>"html")
        );?></h1>
		</div>
		<a href="javascript:void(0);" class="scrolldown">
			<h3><?=GetMessage("POODROBNEE")?></h3>
			<div data-icon="&#xe017;" class="icon"></div>
		</a>
	</div>