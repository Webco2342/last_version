<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("iblock"))
$arSelect = Array(
"ID", 
"NAME", 
"PROPERTY_slaogan", 
"PROPERTY_slogan2",
"PROPERTY_email", 
"PROPERTY_contakt",
"PROPERTY_logo", 
"PREVIEW_PICTURE",
"PREVIEW_TEXT",
"DETAIL_PICTURE",
"DETAIL_TEXT",
"DETAIL_PAGE_URL",
"ACTIVE_DATE" =>"Y"
);
$arFilter = Array("IBLOCK_ID"=>"#DEMO_IBLOCK_ID#", "ID"=>$_GET['ID'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>'1'), $arSelect);
while($ob = $res->GetNextElement()){
$arFild = $ob->GetFields();?>
		<div class="section divider">
		<h2><?=$arFild['NAME']?></h2>		
		</div>
		<div class="content">		
			
				<img style="float: left;margin-right: 10px;" width="340px" src="<?=CFile::GetPath ($arFild["PREVIEW_PICTURE"])?>">			
		
				<?=$arFild['PREVIEW_TEXT']?>
		</div>
		<div class="clear"></div>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>