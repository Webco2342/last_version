<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

CUtil::InitJSCore(array('popup'));

$arResult = array();
if (CModule::IncludeModule("iblock")):
if(!isset($arParams["CACHE_TIME"]))
$arParams["CACHE_TIME"] = 36000000;
$arSelect = Array(
"ID", 
"NAME", 
"PROPERTY_FM", 
"PROPERTY_PROGRAMM",
"PREVIEW_PICTURE",
"PREVIEW_TEXT",
"DETAIL_PAGE_URL",
"PROPERTY_GRIL",
"PROPERTY_URL",
"ACTIVE_DATE" =>"Y"
);
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_TOP_VALUE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>$arParams['IBLOCK_ITEMS_COUNT']), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arResult['ITEMS'][] = $arFields;
};
    $this->IncludeComponentTemplate();
    endif;
?>