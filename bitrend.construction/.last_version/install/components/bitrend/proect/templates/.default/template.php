<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->createFrame()->begin(GetMessage("BITREND_CONSTRUCTION-COMPANY_ZAGRUZKA")); 
?>
	<div class="section divider">
		<h2 id="servicestitle"><?=GetMessage("BITREND_CONSTRUCTION-COMPANY_PROEKTY")?></h2>
		<a class="scroll" id="portfolio"></a>
	</div>
<ul class="portfoliofilter">
<?
$hu = array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"DESC"), Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "CODE"=>"m2"));
while($enum_fields = $property_enums->GetNext())
{?>
  <li class="filter" data-filter="<?=$enum_fields["ID"]?>"><?=$enum_fields["VALUE"]?></li>
  <?$hu[]=$enum_fields["ID"];?>
<?}?>
<li class="filter active" data-filter="<?foreach ($hu as $hus){echo ' '.$hus;}?>"><?=GetMessage("BITREND_CONSTRUCTION-COMPANY_VSE")?></li>

</ul>
	<div class="clear"></div>

	<ul class="portfolio">
                   <?foreach ($arResult["ITEMS"] as $arItems):
					$img = $arItems["PREVIEW_PICTURE"];
					$res = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arItems["ID"], "sort", "asc", array("CODE" => "m2"));
					if ($ob = $res->GetNext())
					{
						$VALUE = $ob['VALUE'];
						$NAME = $ob['VALUE_ENUM'];
						
					}
				   ?>				
 		<li  class="item <?=$VALUE?>">
			<div class="portfolioitem">
				<img width="500px" src="<?=CFile::GetPath ($img)?>">
				<div class="portfoliohover">
					<div class="info">
						<a href="#ob"><h1 id="element<?=$arItems['ID']?>">+</h1></a>
						<h5><?=$arItems["NAME"]?></h5>
						<h6><?=GetMessage("BITREND_CONSTRUCTION-COMPANY_PLOSADQ")?><b class="light-gray"> / </b><?=$NAME?> <?=GetMessage("BITREND_CONSTRUCTION-COMPANY_M")?></h6>
					</div>
				</div>
			</div>
				<script>
				$('#element<?=$arItems['ID']?>').click(function(){					
					 BX.ajax.insertToNode('<?=SITE_DIR?>ob.php?ID=<?=$arItems['ID']?>', 'result'); 
				  });
				</script>
		</li>
                     <?endforeach;?>
	</ul>
	
			<div class="clear"></div>
			<a class="scroll" id="ob"></a>
<div id="result">

</div>	

	