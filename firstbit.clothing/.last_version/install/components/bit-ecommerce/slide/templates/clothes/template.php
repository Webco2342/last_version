<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame("slide", false)->begin();
?>

            <div id="hero" class="homepage-slider2 wow fadeInUp ">
            	<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm m-t-20">
                	

               <?foreach ($arResult["ITEMS"] as $arItems):     

        	   ?>		
		<div class="full-width-slider">	
			<div class="item" style="background-image: url(<?=CFile::GetPath ($arItems["DETAIL_PICTURE"])?>);">
				<div class="container-fluid">
					<div class="caption vertical-center text-left" style="left:<?=$arItems['PROPERTY_IMG_POSITION_LEFT_VALUE']?>; top:<?=$arItems['PROPERTY_IMG_POSITION_TOP_VALUE']?>">
						<div class="big-text fadeInDown-1" style="color:#<?if($arItems['PROPERTY_COLOR_VALUE']==GetMessage('BLACK')){?>000<?}else{?>fff<?}?>;">
							<?=$arItems["NAME"]?>
						</div>

						<div class="excerpt fadeInDown-2" style="color:#<?if($arItems['PROPERTY_COLOR_VALUE']==GetMessage('BLACK')){?>000<?}else{?>fff<?}?>;">
							<?=htmlspecialchars_decode ($arItems["PREVIEW_TEXT"])?>
						</div>
					
						<div class="button-holder hidden-sm fadeInDown-3">
							<a href="<?=$arItems['PROPERTY_URL_VALUE']?>" class="big btn btn-primary" style="color:#<?if($arItems['PROPERTY_COLOR_CLIC_VALUE']==GetMessage('BLACK')){?>000<?}elseif($arItems['PROPERTY_COLOR_CLIC_VALUE']==GetMessage('BLUE')){?>71c9e5<?}else{?>fff<?}?>;"><?=GetMessage('POOO'); ?></a>
						
						<?if($arItems['PROPERTY_ELEMENT_VALUE']==true){
						$res = CIBlockElement::GetByID($arItems['PROPERTY_ELEMENT_VALUE']);
						if($ar_res = $res->GetNext())
						$DETAIL_PAGE_URL = $ar_res['DETAIL_PAGE_URL'];
							?>
						
							<a href="<?=$DETAIL_PAGE_URL?>" class="big btn btn-primary" style="color:#<?if($arItems['PROPERTY_COLOR_CLIC_VALUE']==GetMessage('BLACK')){?>000<?}elseif($arItems['PROPERTY_COLOR_CLIC_VALUE']==GetMessage('BLUE')){?>71c9e5<?}else{?>fff<?}?>; margin-left: 25px;"><?=GetMessage('CLICK'); ?></a>
						<?}?>	
						</div>
						
					</div><!-- /.caption -->
				</div><!-- /.container-fluid -->
			</div><!-- /.item -->
		</div><!-- /.full-width-slider -->   
				<?endforeach;?>

			</div>    
		</div>	
			
		
	<?$frame->end();?>