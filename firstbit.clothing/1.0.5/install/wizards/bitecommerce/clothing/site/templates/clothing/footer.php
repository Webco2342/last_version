<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/footer.php");
?>
</div>
<div style="clear:both"></div>

<div id="brands-carousel" class="logo-slider wow fadeInUp">

		<h3 class="section-title"><?=GetMessage("BRENDS_TITLE")?></h3>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/brends.php"), false);?>
</div>
</div>
</div>
</div>
<footer id="footer" class="footer color-bg">
	  <div class="links-social inner-top-sm">
        <div class="container">
            <div class="row">
            	<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="contact-info">				
    <div class="footer-logo">
        <div class="logo">
            <a href="<?=SITE_DIR?>">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>			
			</a>
        </div><!-- /.logo -->    
    </div><!-- /.footer-logo -->
     <div class="module-body m-t-20">
        <p class="about-us"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/about_footer.php"), false);?></p>	 
						<?
						$facebookLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_facebook.php");
						$twitterLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_twitter.php");
						$googlePlusLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_google.php");
						$linkedinLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_linkedin.php");
						$vkLink = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_vk.php");
						?>
		<div class="social-icons">
								<?if ($facebookLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_facebook.php"), false);?>
								<?endif?>
								<?if ($twitterLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_twitter.php"), false);?>
								<?endif?>
								<?if ($vkLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_vk.php"), false);?>
								<?endif?>
								<?if ($googlePlusLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_google.php"), false);?>
								<?endif?>
								<?if ($linkedinLink):?>
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/socnet_linkedin.php"), false);?>
								<?endif?>
		</div>
	 </div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
<div class="contact-timing">
	<div class="module-heading">
		<h4 class="module-title"><?=GetMessage("TIME_TITLE")?></h4>
	</div><!-- /.module-heading -->

	<div class="module-body outer-top-xs">
		<div class="table-responsive">
			 <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/time_footer.php"), false);?>
								
		</div><!-- /.table-responsive -->
	</div><!-- /.module-body -->
</div>
</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"bottom_menu", 
						array(
							"ROOT_MENU_TYPE" => "bottom",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME" => "site",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"COMPONENT_TEMPLATE" => "top_horizontal"
						),
						false
					);?>
				</div>
            	<div class="col-xs-12 col-sm-6 col-md-3" style="right: -35px;">
            		 <!-- ============================================================= INFORMATION============================================================= -->
<div class="contact-information">
	<div class="module-heading">
		<h4 class="module-title"><?=GetMessage("CONTACT_TITLE")?></h4>
	</div><!-- /.module-heading -->

	<div class="module-body outer-top-xs">
        <ul class="toggle-footer" style="">
            <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <p><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_adres_footer.php"), false);?></p>
                </div>
            </li>

              <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <p><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_phone.php"), false);?></p>
                </div>
            </li>

              <li class="media">
                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <span><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_mail.php"), false);?></span>
                </div>
            </li>
              
            </ul>
    </div><!-- /.module-body -->
</div><!-- /.contact-timing -->
<!-- ============================================================= INFORMATION : END ============================================================= -->            	</div><!-- /.col -->
				
				
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.links-social -->

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="copyright">
							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/copyright.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/pay.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
                    </ul>
                </div><!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>
<div id="hideBlockEmail" style="display:none;">   
    <p>
							<?$APPLICATION->IncludeFile(
                                $APPLICATION->GetTemplatePath(SITE_DIR."include/form_footer.php"),
                                Array(),
                                Array("MODE"=>"html")
                            );?>
	</p>
</div>
<div id="DELAYED" style="display:none;">   
<div style="width: 96%; margin: 10px 2%; text-align: center;"><p><?=GetMessage("S1")?></p></div>
</div>
<script>
   window.BXDEBUG = true;
BX.ready(function(){
   var oPopup = new BX.PopupWindow('call_feedback', window.body, {
      autoHide : true,
      offsetTop : 1,
      offsetLeft : 0,
      lightShadow : true,
      closeIcon : true,
	  titleBar: {content: BX.create("span", {html: '<?=GetMessage("S2")?>', 'props': {'className': 'access-title-bar'}})}, 
      closeByEsc : true,
      overlay: {
         backgroundColor: '#333', opacity: '80'
      }
   });
   oPopup.setContent(BX('hideBlockEmail').innerHTML);
   BX.bindDelegate(
      document.body, 'click', {className: 'phone2' },
         BX.proxy(function(e){
            if(!e)
               e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
         }, oPopup)
   );
   
   
});


window.BXDEBUG = true;
BX.ready(function(){
   var oPopup = new BX.PopupWindow('call_feedback2', window.body, {
      autoHide : true,
      offsetTop : 1,
      offsetLeft : 0,
      lightShadow : true,
      closeIcon : true,
	  titleBar: {content: BX.create("span", {html: '<?=GetMessage("S3")?>', 'props': {'className': 'access-title-bar'}})},
      closeByEsc : true,
      overlay: {
         backgroundColor: '#333', opacity: '80'
      }
   });
   oPopup.setContent(BX('hideBlockEmail').innerHTML);
   BX.bindDelegate(
      document.body, 'click', {className: 'vopros' },
         BX.proxy(function(e){
            if(!e)
               e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
         }, oPopup)
   );
   
   
});

window.BXDEBUG = true;
BX.ready(function(){
   var oPopup = new BX.PopupWindow('DELAYED', window.body, {
      autoHide : true,
      offsetTop : 1,
      offsetLeft : 0,
      lightShadow : true,
      closeIcon : true,
	  titleBar: {content: BX.create("span", {html: '<?=GetMessage("S4")?>', 'props': {'className': 'access-title-bar'}})},
      closeByEsc : true,
      overlay: {
         backgroundColor: '#333', opacity: '80'
      },
	    buttons: [
      new BX.PopupWindowButton({
          text: "Перейти в ибранное",
          className: "main-buttons",
          events: {click: function(){
           location.href="<?=SITE_DIR?>personal/cart/?DELAYED=Y";
          }}
      }),
      new BX.PopupWindowButton({
          text: "Продолжить покупки",
          className: "main-buttons",
          events: {click: function(){
           this.popupWindow.close(); 
          }}
      })
     ]
   });
   
   
   oPopup.setContent(BX('DELAYED').innerHTML);
   BX.bindDelegate(
      document.body, 'click', {className: 'wishlist' },
	    BX.proxy(function(e){
			setTimeout(function() {
            if(!e)
               e = window.event;
            oPopup.show();
            return BX.PreventDefault(e);
			}, 300); 
         }, oPopup)
		  
   );
   
   
});
</script>


	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/bootstrap.min.js"></script>	
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/owl.carousel.min.js"></script>	
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/echo.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jquery.easing-1.3.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/bootstrap-slider.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/jquery.rateit.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/assets/js/lightbox.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/bootstrap-select.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/wow.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/scripts.js"></script>	
    <script src="<?=SITE_TEMPLATE_PATH?>/assets/js/zoomsl-3.0.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/assets/js/zoomsl-3.0.min.js"></script>
	<script>
   jQuery(function(){
   
      $(".cart-foto").imagezoomsl({
         magnifierborder: "1px solid #D6D6D6", 
         zoomrange: [4, 8],
		 magnifiersize: [500, 385]	
      });
   });   
   
</script>

<script>
		$(window).bind("load", function() {
		   $('.show-theme-options').delay(2000).trigger('click');
		});
	</script>
</body>
</html>