<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

CUtil::InitJSCore(array('popup'));

$arResult = array();
if (CModule::IncludeModule("iblock")):
if(!isset($arParams["CACHE_TIME"]))
$arParams["CACHE_TIME"] = 36000000;
$arSelect = Array(
"ID", 
"NAME", 
"CODE", 
"PREVIEW_PICTURE",
"PREVIEW_TEXT",
"DETAIL_PICTURE",
"IBLOCK_ID",
"PROPERTY_COLOR",
"PROPERTY_COLOR_CLIC",
"PROPERTY_ELEMENT",
"PROPERTY_URL",
"PROPERTY_IMG_POSITION_TOP",
"PROPERTY_IMG_POSITION_LEFT",
"PROPERTY_BUT_POSITION_TOP",
"PROPERTY_BUT_POSITION_LEFT",
"PROPERTY_FON_SIZE",
"PROPERTY_CENTER",
"PROPERTY_NO_TEXT",
);
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>$arParams['IBLOCK_ITEMS_COUNT']), $arSelect);
while($ob = $res->GetNextElement()){
$arFields = $ob->GetFields();
$arButtons = CIBlock::GetPanelButtons(
         $arFields["IBLOCK_ID"],
         $arFields["ID"],
         0,
         array("SECTION_BUTTONS"=>false, "SESSID"=>false)
      );
$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
$arResult['ITEMS'][] = $arFields;
};
    $this->IncludeComponentTemplate();
    endif;
?>