<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div style="    position: relative; top: -50px;"> 
			<div class="blog-page">
				<div class="col-md-12">
<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
<div class="blog-post <?if($key!=0){?> outer-top-bd <?}?> wow fadeInUp" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
							class="img-responsive"
							src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
							width="<?=$arItem["DETAIL_PICTURE"]["WIDTH"]?>"
							height="<?=$arItem["DETAIL_PICTURE"]["HEIGHT"]?>"
							alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
							title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
							/></a>
				<?else:?>
					<img
						class="img-responsive"
						src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
						width="<?=$arItem["DETAIL_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["DETAIL_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
						title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
						/>
				<?endif;?>
			<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<h1>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
				<?else:?>
					<?echo $arItem["NAME"]?>
				<?endif;?>
			</h1>
		<?endif;?>
				<?foreach($arItem["FIELDS"] as $code=>$value):?>
			<?if($code == "SHOW_COUNTER"):?>
				<span class="bx-newslist-view"><?=GetMessage("IBLOCK_FIELD_".$code)?>:
					<?=intval($value);?>
				</span>
			<?elseif(
				$value
				&& (
					$code == "SHOW_COUNTER_START"
					|| $code == "DATE_ACTIVE_FROM"
					|| $code == "ACTIVE_FROM"
					|| $code == "DATE_ACTIVE_TO"
					|| $code == "ACTIVE_TO"
					|| $code == "DATE_CREATE"
					|| $code == "TIMESTAMP_X"
				)
			):?>
				<?
				$value = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($value, CSite::GetDateFormat()));
				?>
				<span class="date-time">
					<?=$value;?>
				</span>
			<?elseif($code == "TAGS" && $value):?>
				<span class="bx-newslist-tags">
					<?=$value;?>
				</span>
			<?elseif(
				$value
				&& (
					$code == "CREATED_USER_NAME"
					|| $code == "USER_NAME"
				)
			):?>
				<span class="author"> 
					<?=$value;?>
				</span>
			<?endif;?>
		<?endforeach;?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<p >
			<?echo $arItem["PREVIEW_TEXT"];?>
			</p>
		<?endif;?>

				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<a class="btn btn-upper btn-primary read-more" href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo GetMessage("CT_BNL_GOTO_DETAIL")?></a>
			<?endif;?>
</div>
<?endforeach;?>
</div>
<div class="clearfix blog-pagination filters-container  wow fadeInUp outer-top-bd">
						
	<div class="text-right">
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div></div>
</div>
</div>