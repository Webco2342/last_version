<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);

if($arParams["SHOW_INPUT"] !== "N"):?>
<?
$IBLOCK_ID = array();
$res = CIBlock::GetList(
    Array(), 
    Array(
		'TYPE'=>'catalog_kosmetika',        
        'SITE_ID'=>SITE_ID, 
		"CNT_ACTIVE"=>"Y", 
        'ACTIVE'=>'Y', 
    ), true
);
while($ar_res = $res->Fetch())
{
    $IBLOCK_ID[] =  $ar_res['ID'];
}
?>
<div class="search-area" id="<?echo $CONTAINER_ID?>">
	<form action="<?echo $arResult["FORM_ACTION"]?>">
        <div class="control-group">
            <ul class="categories-filter animate-dropdown">
                <li class="dropdown">

                    <a class="dropdown-toggle"  data-toggle="dropdown" href="<?=$arParams['PAGE']?>"><?=GetMessage("CT_BST_SEARCH_BUTTON_TITLE")?> <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu" >
<?
  $arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'DEPTH_LEVEL'=>1);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  $db_list->NavStart(10);
  while($ar_result = $db_list->GetNext())
  {?>
     <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$ar_result['SECTION_PAGE_URL']?>"><?=$ar_result['NAME']?></a></li>	
  <?}?>
                    </ul>
                </li>
            </ul>
		<input id="<?echo $INPUT_ID?>" type="text" name="q" value="<?=htmlspecialcharsbx($_REQUEST["q"])?>" autocomplete="off" class="search-field"/>
			
				<button class="search-button" type="submit" name="s"></button>
			
		</div>
	</form>
</div>
<?endif?>
<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
			'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
			'INPUT_ID': '<?echo $INPUT_ID?>',
			'MIN_QUERY_LEN': 2
		});
	});
</script>

