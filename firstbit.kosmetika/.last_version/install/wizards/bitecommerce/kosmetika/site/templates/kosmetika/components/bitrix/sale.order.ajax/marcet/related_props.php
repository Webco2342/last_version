<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");

$style = (is_array($arResult["ORDER_PROP"]["RELATED"]) && count($arResult["ORDER_PROP"]["RELATED"])) ? "" : "display:none";
?>
<div style="<?=$style?>" class="panel panel-default checkout-step-22">
	
							    <div class="panel-heading">
						      <h4 class="unicase-checkout-title">
						        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse22">
						          <span><i class="icon fa fa-dollar"></i></span><h4><?=GetMessage("SOA_TEMPL_RELATED_PROPS")?></h4>
						        </a>
						      </h4>
						    </div>
							    <div id="collapse22" class="panel-collapse collapse">
						      <div class="panel-body">
	<?=PrintPropsForm($arResult["ORDER_PROP"]["RELATED"], $arParams["TEMPLATE_LOCATION"])?>
</div></div>
<div class="bx_section">
	<div class="clear"></div>
</div>
</div>