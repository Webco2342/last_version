<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
global $USER;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<?$APPLICATION->ShowHead();?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<?
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/bootstrap.min.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/main.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/owl.carousel.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/owl.transitions.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/lightbox.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/animate.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/rateit.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/bootstrap-select.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/config.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/font-awesome.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/core_fileinput.css");
	$APPLICATION->SetAdditionalCSS("http://fonts.googleapis.com/css?family=Roboto:300,400,500,700");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/assets/css/color.css");
	$APPLICATION->SetTitle("PopUp");
	CJSCore::Init(array("popup"));
	?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
	<body class="cnt-homepage">
		<div id="panel"><?=$APPLICATION->ShowPanel()?></div>
<header class="header-style-1 header-style-2">
<div class="top-bar animate-dropdown">
	<div class="container">
		<div class="header-top-inner">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/top_iner.php"), false);?>
			<div class="clearfix"></div>
		</div><!-- /.header-top-inner -->
	</div><!-- /.container -->
</div><!-- /.header-top -->

	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
					<div class="logo">
						<a href="<?=SITE_DIR?>">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_logo.php"), false);?>
						</a>
					</div><!-- /.logo -->
<!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->
				<div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
					<div class="contact-row">
						<div class="phone inline">
							<i class="icon fa fa-phone"></i> <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_phone.php"), false);?>
						</div>
						<div class="contact inline">
							<i class="icon fa fa-envelope"></i> <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_mail.php"), false);?>
						</div>
						<div class="phone2 inline">
							<i class="icon fa fa-phone"></i> <?=GetMessage("ZACAZ_ZVON")?></div>
					</div><!-- /.contact-row -->	

<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/search.php"), false);?>
				</div>
				<div id="bascet" class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
					<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "main_bascet", array(
	"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "Y",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "N",
		"PATH_TO_REGISTER" => SITE_DIR."login/",
		"PATH_TO_PROFILE" => SITE_DIR."personal/",
		"COMPONENT_TEMPLATE" => "main_bascet",
		"SHOW_EMPTY_VALUES" => "Y",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N",
		"SHOW_IMAGE" => "Y",
		"SHOW_PRICE" => "Y",
		"SHOW_SUMMARY" => "Y",
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
				
				</div>


			</div><!-- /.row -->

		</div><!-- /.container -->

	</div><!-- /.main-header -->
	<div class="header-nav animate-dropdown">
    <div class="container">
	
						<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_horizontal", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "4",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "top_horizontal"
	),
	false
);?>
	
	</div>
	</div>

</header>
	<?if($curPage != SITE_DIR."index.php"){?>
	<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"marcet", 
	array(
		"COMPONENT_TEMPLATE" => "marcet",
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	),
	false
);?>

						<?}?>
<div class="body-content <?if($curPage == SITE_DIR."index.php"){?>outer-top-xs<?}?>" id="top-banner-and-menu">
	<div class="container">
	<div class="homepage-container">
	<?if(($curPage != SITE_DIR."personal/cart/index.php") && ($curPage != SITE_DIR."catalog/compare/index.php") && ($curPage != SITE_DIR."about/contacts/index.php") && ($curPage != SITE_DIR."about/faq/index.php") && ($curPage != SITE_DIR."personal/order/make/index.php") && ($curPage != SITE_DIR."index.php") && ($curPage != SITE_DIR."stores/index.php")){?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "sidebar",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_MODE" => "html",
							),
							false,
							Array('HIDE_ICONS' => 'Y')
						);?>
	<?}?>					
	<div class="col-xs-12 col-sm-12 col-md-<?if(($curPage != SITE_DIR."personal/cart/index.php") && ($curPage != SITE_DIR."catalog/compare/index.php") && ($curPage != SITE_DIR."about/contacts/index.php") && ($curPage != SITE_DIR."about/faq/index.php") && ($curPage != SITE_DIR."personal/order/make/index.php") && ($curPage != SITE_DIR."index.php") && ($curPage != SITE_DIR."stores/index.php")){?>9<?}else{?>12<?}?> homebanner-holder">