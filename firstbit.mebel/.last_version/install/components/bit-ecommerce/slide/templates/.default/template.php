<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame("slide", false)->begin();
?>

            <div id="hero">
            	<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                	

               <?foreach ($arResult["ITEMS"] as $arItems):     

        	   ?>		
		<div class="item" style="min-width:871px; background-image: url(<?=CFile::GetPath ($arItems["DETAIL_PICTURE"])?>);">
			<div class="container-fluid">
			<div class="caption bg-color vertical-center text-left" style="left: <?=$arItems['PROPERTY_IMG_POSITION_LEFT_VALUE']?>px;
    top: <?=$arItems['PROPERTY_IMG_POSITION_TOP_VALUE']?>px;">
			<div class="excerpt fadeInDown-1 hidden-xs">
			<img src="<?=CFile::GetPath ($arItems["PREVIEW_PICTURE"])?>">
			</div>
			</div>
				<div class="caption bg-color vertical-center text-left">
					<div class="big-text fadeInDown-1" style="color:#<?if($arItems['PROPERTY_COLOR_VALUE']=='Черный'){?>000<?}else{?>fff<?}?>;     max-width: 500px; min-width:450px;">
						<?=$arItems["NAME"]?>
					</div>

					<div class="excerpt fadeInDown-2 hidden-xs">
					
					<span style="    font-size: 18px; color:#<?if($arItems['PROPERTY_COLOR_VALUE']=='Черный'){?>000<?}else{?>fff<?}?>"><?=htmlspecialchars_decode ($arItems["PREVIEW_TEXT"])?></span>
					</div>
					<?if($arItems['PROPERTY_URL_VALUE']){?>
					<div class="button-holder fadeInDown-3" style="opacity: 1; top: 0px;">
						<a href="<?=$arItems['PROPERTY_URL_VALUE']?>" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Подробнее</a>
					</div>
					<?}?>
				</div><!-- /.caption -->
			</div><!-- /.container-fluid -->
		</div><!-- /.item -->	   
				<?endforeach;?>

			</div>    
		</div>	
			
	<?$frame->end();?>