<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!CModule::IncludeModule("highloadblock"))
	return;

if (!WIZARD_INSTALL_DEMO_DATA)
	return;

$COLOR_ID = $_SESSION["HBLOCK_COLOR_ID"];
unset($_SESSION["HBLOCK_COLOR_ID"]);

$BRAND_ID = $_SESSION["HBLOCK_BRAND_ID"];
unset($_SESSION["HBLOCK_BRAND_ID"]);

$MARCETION_ID = $_SESSION["HBLOCK_MARCETION_ID"];
unset($_SESSION["HBLOCK_MARCETION_ID"]);
//adding rows
WizardServices::IncludeServiceLang("references.php", LANGUAGE_ID);

use Bitrix\Highloadblock as HL;
global $USER_FIELD_MANAGER;

if ($COLOR_ID)
{
	$hldata = HL\HighloadBlockTable::getById($COLOR_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arColors = array(
		"COLOR1" => "references_files/color1.jpg",
		"COLOR2" => "references_files/color2.jpg",
		"COLOR3" => "references_files/color3.jpg",
		"COLOR4" => "references_files/color4.jpg",
		"COLOR5" => "references_files/color5.jpg",
		"COLOR6" => "references_files/color6.jpg",
		"COLOR7" => "references_files/color7.jpg",
		"COLOR8" => "references_files/color8.jpg",
		"COLOR9" => "references_files/color9.jpg",
		"COLOR10" => "references_files/color10.jpg",

	);
	$sort = 0;
	foreach($arColors as $colorName=>$colorFile)
	{
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_COLOR_".$colorName),
			'UF_FILE' =>
				array (
					'name' => ToLower($colorName).".jpg",
					'type' => 'image/jpeg',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/iblock/".$colorFile
				),
			'UF_SORT' => $sort,
			'UF_DEF' => ($sort > 100) ? "0" : "1",
			'UF_XML_ID' => ToLower($colorName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$COLOR_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$COLOR_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}

if ($BRAND_ID)
{
	$hldata = HL\HighloadBlockTable::getById($BRAND_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arBrands = array(
		"STOPLPLIT" => "brands_files/cm-03.png",
		"MARIA" => "brands_files/cm-02.png",
		"SHATURA" => "brands_files/cm-01.png",
	);
	$sort = 0;
	foreach($arBrands as $brandName=>$brandFile)
	{
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_BRAND_".$brandName),
			'UF_FILE' =>
				array (
					'name' => ToLower($brandName).".png",
					'type' => 'image/png',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/iblock/".$brandFile
				),
			'UF_SORT' => $sort,
			'UF_DESCRIPTION' => GetMessage("WZD_REF_BRAND_DESCR_".$brandName),
			'UF_FULL_DESCRIPTION' => GetMessage("WZD_REF_BRAND_FULL_DESCR_".$brandName),
			'UF_XML_ID' => ToLower($brandName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$BRAND_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$BRAND_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
	

}
if ($MARCETION_ID)
{
	$hldata = HL\HighloadBlockTable::getById($MARCETION_ID)->fetch();
	$hlentity = HL\HighloadBlockTable::compileEntity($hldata);

	$entity_data_class = $hlentity->getDataClass();
	$arBrands = array(
		"TRUCK" => "brands_files/truck.gif",
		"GIFT" => "brands_files/gift.gif",
		"DOLLAR" => "brands_files/dollar.gif",

	);
	$sort = 0;
	foreach($arMarcetion as $MarcetionName=>$MarcetionFile)
	{
		$sort+= 100;
		$arData = array(
			'UF_NAME' => GetMessage("WZD_REF_MARCETION_".$brandName),
			'UF_FILE' =>
				array (
					'name' => ToLower($MarcetionName).".gif",
					'type' => 'image/gif',
					'tmp_name' => WIZARD_ABSOLUTE_PATH."/site/services/iblock/".$MarcetionFile
				),
			'UF_SORT' => $sort,
			'UF_DESCRIPTION' => GetMessage("WZD_REF_MARCETION_DESCR_".$MarcetionName),
			'UF_FULL_DESCRIPTION' => GetMessage("WZD_REF_MARCETION_FULL_DESCR_".$MarcetionName),
			'UF_XML_ID' => ToLower($brandName)
		);
		$USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_'.$MARCETION_ID, $arData);
		$USER_FIELD_MANAGER->checkFields('HLBLOCK_'.$MARCETION_ID, null, $arData);

		$result = $entity_data_class::add($arData);
	}
}
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/include/brends.php", array("BRAND_ID" => $BRAND_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/brands/index.php", array("BRAND_ID" => $BRAND_ID));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/brands/detail.php", array("BRAND_ID" => $BRAND_ID));
?>