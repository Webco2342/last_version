<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wide-banners wow fadeInUp homepage-banner outer-bottom-small">
	<div class="row">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?><a href="<?=$arItem['PROPERTIES']['HTTP']['VALUE']?>"><?endif;?>
			
		<div class="col-md-4">
			<div class="wide-banner cnt-strip">
				<div class="image">
					<img class="img-responsive" data-echo="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" src="<?=SITE_TEMPLATE_PATH?>/assets/images/blank.gif" alt="">
				</div>	
				<div class="strip">
					<div class="strip-inner">
						<h4 class="white"><?echo $arItem["NAME"]?></h4>
						<h3 class="white"><?echo $arItem["PREVIEW_TEXT"];?></h3>
					</div>	
				</div>
			</div><!-- /.wide-banner -->
		</div><!-- /.col -->
			
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?></a><?endif;?>
<?endforeach;?>
</div>
</div>

