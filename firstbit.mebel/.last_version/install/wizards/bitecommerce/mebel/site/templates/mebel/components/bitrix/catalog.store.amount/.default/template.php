<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

	<?if(!empty($arResult["STORES"])):?>
	
<div class="item_info_section">	
	<table class="detail_table">
<tbody>
		<?foreach($arResult["STORES"] as $pid => $arProperty):?>

<tr>
		<td class="dd"><span><a href="<?=$arProperty['URL']?>"> <?=$arProperty['TITLE']?></a> <?if($arProperty['PHONE']){?><?=GetMessage("S_PHONE")?> <?=$arProperty['PHONE']?><?}?></span></td><td class="dt"><span><?if(($arProperty['REAL_AMOUNT']>='1') && ($arProperty['REAL_AMOUNT']<'10')){?><i style="color:rgb(68, 187, 110)" class="fa fa-check"></i> <?=GetMessage('S_AMOUNT_1')?><?}elseif($arProperty['REAL_AMOUNT']>='10'){?><i style="color:rgb(68, 187, 110)" class="fa fa-check"></i> <?=GetMessage('S_AMOUNT_10')?><?}elseif($arProperty['REAL_AMOUNT']<'1'){?><i style="color:red" class="fa fa-check"></i> <?=GetMessage('S_AMOUNT_0')?><?}?></span></td>
</tr>

			
		<?endforeach;?>
</tbody></table>
</div>
	<?endif;?>

